package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.ErrorInfo;
import com.fuxiang.web.service.NewsCategoryService;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.NewsCategoryDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "新闻分类")
@RestController
@RequestMapping(value = "/newsCategory")
public class NewsCategoryController {

    @Autowired
    private NewsCategoryService newsCategoryService;

    @ApiOperation(
            value = "新增新闻分类",
            notes = "<b>必传属性：</b><br/>" +
                    "1.name 新闻分类名称<br/>" +
                    "2.leaf 是否是叶子节点<br/>" +
                    "<i>根节点parentId = root</i>")
    @PostMapping(value = "/add")
    public ActionResult<String> add(@ApiParam("新闻分类对象") @RequestBody NewsCategoryDTO newsCategoryDTO) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(newsCategoryDTO.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(newsCategoryDTO.getParentId()))
            errors.add(new ErrorInfo(0, "父节点不能为空"));
        if (newsCategoryDTO.getLeaf() == null)
            errors.add(new ErrorInfo(0, "叶子节点属性值为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return newsCategoryService.add(newsCategoryDTO);
    }

    @ApiOperation(value = "删除新闻分类")
    @DeleteMapping(value = "/delete/{id}")
    public ActionResult<String> delete(@ApiParam("新闻分类id") @PathVariable String id) {
        return newsCategoryService.delete(id);
    }

    @ApiOperation(
            value = "修改新闻分类",
            notes = "<b>必传属性：</b><br/>" +
                    "1.id 主键<br/>" +
                    "2.name 新闻分类名称<br/>" +
                    "<i>leaf 叶子节点属性不可以修改</i><br/>" +
                    "<i>根节点parentId = root</i>")
    @PutMapping(value = "/update")
    public ActionResult<String> update(@ApiParam("新闻分类对象") @RequestBody NewsCategoryDTO newsCategoryDTO) {
        /*验证数据*/
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(newsCategoryDTO.getId()))
            errors.add(new ErrorInfo(0, "id为空"));
        if (StringUtils.isEmpty(newsCategoryDTO.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(newsCategoryDTO.getParentId()))
            errors.add(new ErrorInfo(0, "父节点不能为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return newsCategoryService.update(newsCategoryDTO);
    }

    @ApiOperation(value = "获取新闻分类")
    @GetMapping(value = "/get/{id}")
    public ActionResult<NewsCategoryDTO> get(@ApiParam("新闻分类id") @PathVariable String id) {
        return newsCategoryService.get(id);
    }

    @ApiOperation("升序")
    @PutMapping("/up/{id}")
    public ActionResult<String> up(@ApiParam("id") @PathVariable String id) {
        return newsCategoryService.up(id);
    }

    @ApiOperation("降序")
    @PutMapping("/down/{id}")
    public ActionResult<String> down(@ApiParam("id") @PathVariable String id) {
        return newsCategoryService.down(id);
    }

    @ApiOperation(value = "查询指定父节点id下的所有分类", notes = "parentId = null 查询所有分类，parentId = root 查询顶级分类")
    @GetMapping(value = "/list")
    public ActionResult<List<NewsCategoryDTO>> list(@ApiParam("父节点id") @RequestParam(required = false) String parentId) {
        return newsCategoryService.list(parentId);
    }

    @ApiOperation(value = "分页查询指定父节点id下的所有分类", notes = "parentId = null 查询所有分类，parentId = root 查询顶级分类")
    @GetMapping(value = "/page")
    public ActionResult<Pagination<NewsCategoryDTO>> get(@ApiParam("父节点id") @RequestParam(required = false) String parentId,
                                                            @ApiParam("页码") @RequestParam Integer pageNo,
                                                            @ApiParam("大小") @RequestParam Integer pageSize) {
        if (pageNo != null && pageNo <= 0)
            pageNo = 1;
        if (pageSize != null && pageSize <= 1)
            pageSize = 10;
        return newsCategoryService.page(parentId, pageNo, pageSize);
    }

    @ApiOperation(value = "刷新指定分类下的子分类排序", notes = "顶级节点id = root")
    @PutMapping("/refresh/{id}")
    public ActionResult<String> refresh(@ApiParam("id") @PathVariable String id) {
        return newsCategoryService.refresh(id);
    }

    @ApiOperation("获取所有分类")
    @GetMapping("/listAll")
    public ActionResult<List<NewsCategoryDTO>> listAll(){
        return newsCategoryService.listAll();
    }
}
