package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dto.LinkDTO;
import com.fuxiang.web.common.ErrorInfo;
import com.fuxiang.web.service.LinkService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "友情链接")
@RestController
@RequestMapping("/link")
public class LinkController {
    @Autowired
    private LinkService linkService;

    @ApiOperation(
            value = "添加",
            notes = "<b>必传字段：</b><br/>" +
                    "1.name 链接名称<br/>" +
                    "2.url 链接地址")
    @PostMapping(value = "/add")
    public ActionResult<String> add(@ApiParam("友情链接DTO") @RequestBody LinkDTO linkDTO) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(linkDTO.getName()))
            errors.add(new ErrorInfo(0, "名称不能为空"));
        if (StringUtils.isEmpty(linkDTO.getUrl()))
            errors.add(new ErrorInfo(0, "链接不能为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return linkService.add(linkDTO);
    }

    @ApiOperation("删除")
    @DeleteMapping(value = "/delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id) {
        return linkService.delete(id);
    }

    @ApiOperation(
            value = "修改",
            notes = "<b>必传字段：</b><br/>" +
                    "1.id 主键<br/>" +
                    "2.name 链接名称<br/>" +
                    "3.url 链接地址")
    @PutMapping(value = "/update")
    public ActionResult<String> update(@ApiParam("友情链接DTO") @RequestBody LinkDTO linkDTO) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(linkDTO.getId()))
            errors.add(new ErrorInfo(0, "id不能为空"));
        if (StringUtils.isEmpty(linkDTO.getName()))
            errors.add(new ErrorInfo(0, "名称不能为空"));
        if (StringUtils.isEmpty(linkDTO.getUrl()))
            errors.add(new ErrorInfo(0, "链接不能为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return linkService.update(linkDTO);
    }

    @ApiOperation("获取")
    @GetMapping(value = "/get/{id}")
    public ActionResult<LinkDTO> get(@ApiParam("id") @PathVariable String id) {
        return linkService.get(id);
    }

    @ApiOperation("获取所有列表(无分页)")
    @GetMapping(value = "/list")
    public ActionResult<List<LinkDTO>> list() {
        return linkService.list();
    }

    @ApiOperation("批量删除")
    @DeleteMapping("/batchDelete")
    public ActionResult<List<String>> batchDelete(@ApiParam("id数组") @RequestParam List<String> ids) {
        return linkService.batchDelete(ids);
    }

    @ApiOperation("刷新排序")
    @PutMapping("/refresh")
    public ActionResult refresh() {

        return linkService.refresh();
    }

    @ApiOperation("升序")
    @PutMapping("/up/{id}")
    public ActionResult<String> up(@ApiParam("id") @PathVariable String id) {
        return linkService.up(id);
    }

    @ApiOperation("降序")
    @PutMapping("/down/{id}")
    public ActionResult<String> down(@ApiParam("id") @PathVariable String id) {
        return linkService.down(id);
    }

}
