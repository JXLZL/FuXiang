package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.ErrorInfo;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.NewsDTO;
import com.fuxiang.web.dto.query.NewsQueryDTO;
import com.fuxiang.web.service.NewsService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "新闻")
@RestController
@RequestMapping(value = "/news")
public class NewsController {

    @Autowired
    private NewsService newsService;

    @ApiOperation(
            value = "新增新闻",
            notes = "<b>必填属性：</b><br>" +
                    "1.name 名称<br/>" +
                    "2.categoryId 新闻分类id<i><b>如果没有分类则categoryId = root</b></i><br/>"
    )
    @PostMapping(value = "/add")
    public ActionResult<String> add(@ApiParam("新闻DTO") @RequestBody NewsDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(dto.getCategoryId()))
            errors.add(new ErrorInfo(0, "新闻分类id为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return newsService.add(dto);
    }

    @ApiOperation(value = "删除新闻")
    @DeleteMapping(value = "/delete/{id}")
    public ActionResult<String> delete(@ApiParam("新闻id") @PathVariable String id) {
        return newsService.delete(id);
    }

    @ApiOperation(value = "批量删除新闻")
    @DeleteMapping(value = "/batchDelete")
    public ActionResult<List<String>> batchDelete(@ApiParam("新闻id数组") @RequestParam List<String> ids) {
        if (CollectionUtils.isEmpty(ids))
            return new ActionResult<>(0, "参数不能为空");
        return newsService.batchDelete(ids);
    }

    @ApiOperation(
            value = "修改新闻",
            notes = "<b>必填属性：</b><br>" +
                    "1.id 主键<br/>" +
                    "2.name 名称<br/>" +
                    "3.categoryId 新闻分类id<i><b>如果没有分类则categoryId = root</b></i><br/>"
    )
    @PutMapping(value = "/update")
    public ActionResult<String> update(@ApiParam("新闻DTO") @RequestBody NewsDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0, "id为空"));
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(dto.getCategoryId()))
            errors.add(new ErrorInfo(0, "新闻分类id为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return newsService.update(dto);
    }

    @ApiOperation(value = "获取新闻")
    @GetMapping(value = "get/{id}")
    public ActionResult<NewsDTO> get(@ApiParam("新闻id") @PathVariable String id) {
        return newsService.get(id);
    }

    @ApiOperation("升序")
    @PutMapping("/up/{id}")
    public ActionResult<String> up(@ApiParam("id") @PathVariable String id) {
        return newsService.up(id);
    }

    @ApiOperation("降序")
    @PutMapping("/down/{id}")
    public ActionResult<String> down(@ApiParam("id") @PathVariable String id) {
        return newsService.down(id);
    }

    @ApiOperation(value = "刷新指定新闻分类下的新闻排序")
    @PutMapping(value = "/refresh/{categoryId}")
    public ActionResult<String> refresh(@ApiParam("新闻分类id") @PathVariable String categoryId) {
        return newsService.refresh(categoryId);
    }

    @ApiOperation("分页查询指定条件的新闻")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<NewsDTO>> pageQuery(@ApiParam("新闻查询DTO") @ModelAttribute NewsQueryDTO queryDTO) {
        //验证页码
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return newsService.listByQueryDTO(queryDTO);
    }
}
