package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.ErrorInfo;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.ProductCategoryDTO;
import com.fuxiang.web.service.ProductCategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "产品分类")
@RestController
@RequestMapping(value = "/productCategory")
public class ProductCategoryController {

    @Autowired
    private ProductCategoryService productCategoryService;

    @ApiOperation(
            value = "新增产品分类",
            notes = "<b>必传属性：</b><br/>" +
                    "1.name 产品分类名称<br/>" +
                    "2.leaf 是否是叶子节点<br/>" +
                    "<i>根节点parentId = root</i>")
    @PostMapping(value = "/add")
    public ActionResult<String> add(@ApiParam("产品分类对象") @RequestBody ProductCategoryDTO productCategoryDTO) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(productCategoryDTO.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(productCategoryDTO.getParentId()))
            errors.add(new ErrorInfo(0, "父节点不能为空"));
        if (productCategoryDTO.getLeaf() == null)
            errors.add(new ErrorInfo(0, "叶子节点属性值为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return productCategoryService.add(productCategoryDTO);
    }

    @ApiOperation(value = "删除产品分类")
    @DeleteMapping(value = "/delete/{id}")
    public ActionResult<String> delete(@ApiParam("产品分类id") @PathVariable String id) {
        return productCategoryService.delete(id);
    }

    @ApiOperation(
            value = "修改产品分类",
            notes = "<b>必传属性：</b><br/>" +
                    "1.id 主键<br/>" +
                    "2.name 产品分类名称<br/>" +
                    "<i>leaf 叶子节点属性不可以修改</i><br/>" +
                    "<i>根节点parentId = root</i>")
    @PutMapping(value = "/update")
    public ActionResult<String> update(@ApiParam("产品分类对象") @RequestBody ProductCategoryDTO productCategoryDTO) {
        /*验证数据*/
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(productCategoryDTO.getId()))
            errors.add(new ErrorInfo(0, "id为空"));
        if (StringUtils.isEmpty(productCategoryDTO.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(productCategoryDTO.getParentId()))
            errors.add(new ErrorInfo(0, "父节点不能为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return productCategoryService.update(productCategoryDTO);
    }

    @ApiOperation(value = "获取产品分类")
    @GetMapping(value = "/get/{id}")
    public ActionResult<ProductCategoryDTO> get(@ApiParam("产品分类id") @PathVariable String id) {
        return productCategoryService.get(id);
    }

    @ApiOperation("升序")
    @PutMapping("/up/{id}")
    public ActionResult<String> up(@ApiParam("id") @PathVariable String id) {
        return productCategoryService.up(id);
    }

    @ApiOperation("降序")
    @PutMapping("/down/{id}")
    public ActionResult<String> down(@ApiParam("id") @PathVariable String id) {
        return productCategoryService.down(id);
    }

    @ApiOperation(value = "查询指定父节点id下的所有分类", notes = "parentId = null 查询所有分类，parentId = root 查询顶级分类")
    @GetMapping(value = "/list")
    public ActionResult<List<ProductCategoryDTO>> list(@ApiParam("父节点id") @RequestParam(required = false) String parentId) {
        return productCategoryService.list(parentId);
    }

    @ApiOperation(value = "分页查询指定父节点id下的所有分类", notes = "parentId = null 查询所有分类，parentId = root 查询顶级分类")
    @GetMapping(value = "/page")
    public ActionResult<Pagination<ProductCategoryDTO>> get(@ApiParam("父节点id") @RequestParam(required = false) String parentId,
                                                            @ApiParam("页码") @RequestParam Integer pageNo,
                                                            @ApiParam("大小") @RequestParam Integer pageSize) {
        if (pageNo != null && pageNo <= 0)
            pageNo = 1;
        if (pageSize != null && pageSize <= 1)
            pageSize = 10;
        return productCategoryService.page(parentId, pageNo, pageSize);
    }

    @ApiOperation(value = "刷新指定分类下的子分类排序", notes = "顶级节点id = root")
    @PutMapping("/refresh/{id}")
    public ActionResult<String> refresh(@ApiParam("id") @PathVariable String id) {
        return productCategoryService.refresh(id);
    }

    @ApiOperation("获取所有分类")
    @GetMapping("/listAll")
    public ActionResult<List<ProductCategoryDTO>> listAll(){
        return productCategoryService.listAll();
    }
}
