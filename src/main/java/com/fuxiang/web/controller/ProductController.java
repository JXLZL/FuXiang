package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.ErrorInfo;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.ProductDTO;
import com.fuxiang.web.dto.query.ProductQueryDTO;
import com.fuxiang.web.service.ProductService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "产品")
@RestController
@RequestMapping(value = "/product")
public class ProductController {

    @Autowired
    private ProductService productService;

    @ApiOperation(
            value = "新增产品",
            notes = "<b>必填属性：</b><br>" +
                    "1.name 名称<br/>" +
                    "2.categoryId 产品分类id<i><b>如果没有分类则categoryId = root</b></i><br/>"
    )
    @PostMapping(value = "/add")
    public ActionResult<String> add(@ApiParam("产品对象") @RequestBody ProductDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(dto.getCategoryId()))
            errors.add(new ErrorInfo(0, "产品分类id为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return productService.add(dto);
    }

    @ApiOperation(value = "删除产品")
    @DeleteMapping(value = "/delete/{id}")
    public ActionResult<String> delete(@ApiParam("产品id") @PathVariable String id) {
        return productService.delete(id);
    }

    @ApiOperation(value = "批量删除产品")
    @DeleteMapping(value = "/batchDelete")
    public ActionResult<List<String>> batchDelete(@ApiParam("产品id数组") @RequestParam List<String> ids){
        if (CollectionUtils.isEmpty(ids))
            return new ActionResult<>(0,"参数不能为空");
        return productService.batchDelete(ids);
    }

    @ApiOperation(
            value = "修改产品",
            notes = "<b>必填属性：</b><br>" +
                    "1.id 主键<br/>" +
                    "2.name 名称<br/>" +
                    "3.categoryId 产品分类id<i><b>如果没有分类则categoryId = root</b></i><br/>"
    )
    @PutMapping(value = "/update")
    public ActionResult<String> update(@ApiParam("产品对象") @RequestBody ProductDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0, "id为空"));
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(dto.getCategoryId()))
            errors.add(new ErrorInfo(0, "产品分类id为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return productService.update(dto);
    }

    @ApiOperation(value = "获取产品")
    @GetMapping(value = "get/{id}")
    public ActionResult<ProductDTO> get(@ApiParam("产品id") @PathVariable String id) {
        return productService.get(id);
    }

    @ApiOperation("升序")
    @PutMapping("/up/{id}")
    public ActionResult<String> up(@ApiParam("id") @PathVariable String id) {
        return productService.up(id);
    }

    @ApiOperation("降序")
    @PutMapping("/down/{id}")
    public ActionResult<String> down(@ApiParam("id") @PathVariable String id) {
        return productService.down(id);
    }

    @ApiOperation(value = "刷新指定产品分类下的产品排序")
    @PutMapping(value = "/refresh/{categoryId}")
    public ActionResult<String> refresh(@ApiParam("产品分类id") @PathVariable String categoryId) {
        return productService.refresh(categoryId);
    }

    @ApiOperation("分页查询指定条件的产品")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<ProductDTO>> listByQueryDTO(@ApiParam("产品查询DTO") @ModelAttribute ProductQueryDTO queryDTO) {
        //验证页码
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return productService.listByQueryDTO(queryDTO);
    }

    //region 这是测试页面伪静态化的
    /*@ApiOperation("查询网页")
    @GetMapping("/{id}.html")
    public void getHtml(@ApiParam("id") @PathVariable String id, HttpServletResponse response) throws IOException {
        System.out.println("进入查询网页...");
        response.setContentType("text/html;charset=utf-8");
        PrintWriter writer = response.getWriter();
        writer.println("你好，HTML");
        writer.flush();
    }

    @ApiOperation("查询网页")
    @GetMapping("user/{id}.html")
    public ModelAndView getUserHtml(@ApiParam("id") @PathVariable String id, HttpServletResponse response) throws IOException {
        System.out.println("进入user查询网页...");
        User user = new User();
        user.setId(1);
        user.setName("张珊珊");
        user.setAge(19);
        ModelAndView modelAndView = new ModelAndView("user");
        modelAndView.addObject("user",user);
        return modelAndView;
    }*/
    //endregion

}
