package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.ImageDTO;
import com.fuxiang.web.service.ImageService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

@Api(tags = "图片")
@RestController
@RequestMapping("/image")
public class ImageController {

    private static final long MAX_SIZE = 1024 * 512;

    private static final Set<String> IMAGE_SUFFIX_SET = new HashSet<>(Arrays.asList(
            "jpg", "JPG", "jpge", "JPGE", "png", "PNG", "gif", "GIF"
    ));

    @Autowired
    private ImageService imageService;

    @ApiOperation(value = "上传图片")
    @PostMapping(value = "/upload")
    public ActionResult<ImageDTO> upload(@ApiParam("图片") @RequestParam("image") MultipartFile file,
                                         @ApiParam("所属模块") @RequestParam String module) {

//        System.out.println("文件限制大小：" + size + "Bytes");
        if (file.getSize() > MAX_SIZE)
            return new ActionResult<>(0, "图片大小超过限制");
        if (file.isEmpty())
            return new ActionResult<>(0, "文件不存在");
        int lastIndex = file.getOriginalFilename().lastIndexOf('.');
        if (lastIndex == -1)
            return new ActionResult<>(0, "文件名称错误，缺失文件后缀");
        String suffix = file.getOriginalFilename().substring(lastIndex + 1);
        if (StringUtils.isEmpty(suffix))
            return new ActionResult<>(0, "文件名称错误，缺失文件后缀");
        if (!IMAGE_SUFFIX_SET.contains(suffix))
            return new ActionResult<>(0, "文件格式不支持，仅支持jpg,jpge,png,gif格式");

        if (StringUtils.isEmpty(module))
            return new ActionResult<>(0, "模块为空");
        return imageService.upload(file, module);
    }

    @ApiOperation(value = "删除图片")
    @DeleteMapping("/delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id) {
        return imageService.delete(id);
    }

    @ApiOperation(value = "收藏图片")
    @PutMapping("/collect/{id}")
    public ActionResult<String> collect(@ApiParam("id") @PathVariable String id) {
        return imageService.collect(id);
    }

    @ApiOperation(value = "引用图片")
    @PutMapping("/ref/{id}")
    public ActionResult<String> ref(@ApiParam("id") @PathVariable String id) {
        return imageService.ref(id);
    }

    @ApiOperation(value = "获取图片")
    @GetMapping("/get/{id}")
    public ActionResult<ImageDTO> get(@ApiParam("id") @PathVariable String id) {
        return imageService.get(id);
    }

    @ApiOperation(value = "分页查询图片")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<ImageDTO>> pageQuery(@ApiParam("所属模块") @RequestParam(required = false) String module,
                                                        @ApiParam("是否收藏") @RequestParam(required = false) Boolean collected,
                                                        @ApiParam("页码") @RequestParam(required = false) Integer pageNo,
                                                        @ApiParam("数量") @RequestParam(required = false) Integer pageSize) {
        if (pageNo == null || pageNo <= 0)
            pageNo = 1;
        if (pageSize == null || pageSize <= 0)
            pageSize = 10;
        return imageService.pageQuery(module, collected, pageNo, pageSize);
    }
}
