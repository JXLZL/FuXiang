package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dto.query.MessageQueryDTO;
import com.fuxiang.web.service.MessageService;
import com.fuxiang.web.common.ErrorInfo;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.MessageDTO;
import com.fuxiang.web.util.EntityUtils;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "留言")
@RestController
@RequestMapping("/message")
public class MessageController {
    @Autowired
    private MessageService messageService;

    @ApiOperation(
            value = "添加留言",
            notes = "<b>必填属性：</b><br>" +
                    "1.id 主键32位UUID，调用/message/uuid获取<br/>" +
                    "2.name 名称<br/>" +
                    "3.tel 联系电话<br/>" +
                    "4.email 电子邮箱<br/>" +
                    "5.content 留言内容<br/>" +
                    "<i style='color:red;font-weight:600'>注意，这里id是由前端传给后台的</i><br>"
    )
    @PostMapping(value = "/add")
    public ActionResult<String> add(@ApiParam("留言DTO") @RequestBody MessageDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0, "id为空"));
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "姓名为空"));
        if (StringUtils.isEmpty(dto.getTel()))
            errors.add(new ErrorInfo(0, "联系电话为空"));
        if (StringUtils.isEmpty(dto.getEmail()))
            errors.add(new ErrorInfo(0, "电子邮箱为空"));
        if (StringUtils.isEmpty(dto.getContent()))
            errors.add(new ErrorInfo(0, "留言内容为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return messageService.add(dto);
    }

    @ApiOperation("删除留言")
    @DeleteMapping(value = "/delete/{id}")
    public ActionResult<String> delete(@ApiParam("id") @PathVariable String id) {
        return messageService.delete(id);
    }

    @ApiOperation("批量删除")
    @DeleteMapping("/batchDelete")
    public ActionResult<List<String>> batchDelete(@ApiParam("留言id数组") @RequestParam List<String> ids) {
        return messageService.batchDelete(ids);
    }

    //region 不提供修改的接口
    /*@ApiOperation("修改")
    @PutMapping(value = "/update")
    public ActionResult<String> update(@ApiParam("留言DTO") @RequestBody MessageDTO messageDTO) {
        return messageService.update(messageDTO);
    }*/
    //endregion

    @ApiOperation("获取留言")
    @GetMapping(value = "/get/{id}")
    public ActionResult<MessageDTO> get(@ApiParam("id") @PathVariable String id) {
        return messageService.get(id);
    }

    @ApiOperation("分页查询指定条件的留言")
    @GetMapping(value = "/pageQuery")
    public ActionResult<Pagination<MessageDTO>> list(@ApiParam("留言查询DTO") @ModelAttribute MessageQueryDTO queryDTO) {
        return messageService.listByQueryDTO(queryDTO);
    }


    @ApiOperation("标记处理")
    @PutMapping("/deal/{id}")
    public ActionResult<String> deal(@ApiParam("id") @PathVariable String id) {
        return messageService.deal(id);
    }

    @ApiOperation("批量标记处理")
    @PutMapping("/batchDeal")
    public ActionResult<List<String>> batchDeal(@ApiParam("ids") @RequestParam List<String> ids) {
        return messageService.batchDeal(ids);
    }

    @ApiOperation("获取32位UUID")
    @GetMapping("/uuid")
    public ActionResult<String> getUUID() {
        return new ActionResult<>(EntityUtils.uuid());
    }
}
