package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dto.NavigationDTO;
import com.fuxiang.web.service.NavigationService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "导航")
@RestController
@RequestMapping("/navigation")
public class NavigationController {

    @Autowired
    private NavigationService navigationService;

    @ApiOperation(value = "获取导航项（新闻分类、产品分类、案例分类）")
    @GetMapping("/getAll")
    public ActionResult<NavigationDTO> getAll() {
        return navigationService.get();
    }
}
