package com.fuxiang.web.controller;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.ErrorInfo;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.ExampleDTO;
import com.fuxiang.web.dto.query.ExampleQueryDTO;
import com.fuxiang.web.service.ExampleService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Api(tags = "案例")
@RestController
@RequestMapping(value = "/example")
public class ExampleController {

    @Autowired
    private ExampleService exampleService;

    @ApiOperation(
            value = "新增案例",
            notes = "<b>必填属性：</b><br>" +
                    "1.name 名称<br/>" +
                    "2.categoryId 案例分类id<i><b>如果没有分类则categoryId = root</b></i><br/>"
    )
    @PostMapping(value = "/add")
    public ActionResult<String> add(@ApiParam("案例DTO") @RequestBody ExampleDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(dto.getCategoryId()))
            errors.add(new ErrorInfo(0, "案例分类id为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return exampleService.add(dto);
    }

    @ApiOperation(value = "删除案例")
    @DeleteMapping(value = "/delete/{id}")
    public ActionResult<String> delete(@ApiParam("案例id") @PathVariable String id) {
        return exampleService.delete(id);
    }

    @ApiOperation(value = "批量删除案例")
    @DeleteMapping(value = "/batchDelete")
    public ActionResult<List<String>> batchDelete(@ApiParam("案例id数组") @RequestParam List<String> ids) {
        if (CollectionUtils.isEmpty(ids))
            return new ActionResult<>(0, "参数不能为空");
        return exampleService.batchDelete(ids);
    }

    @ApiOperation(
            value = "修改案例",
            notes = "<b>必填属性：</b><br>" +
                    "1.id 主键<br/>" +
                    "2.name 名称<br/>" +
                    "3.categoryId 案例分类id<i><b>如果没有分类则categoryId = root</b></i><br/>"
    )
    @PutMapping(value = "/update")
    public ActionResult<String> update(@ApiParam("案例DTO") @RequestBody ExampleDTO dto) {
        List<ErrorInfo> errors = new ArrayList<>();
        if (StringUtils.isEmpty(dto.getId()))
            errors.add(new ErrorInfo(0, "id为空"));
        if (StringUtils.isEmpty(dto.getName()))
            errors.add(new ErrorInfo(0, "名称为空"));
        if (StringUtils.isEmpty(dto.getCategoryId()))
            errors.add(new ErrorInfo(0, "案例分类id为空"));
        if (!errors.isEmpty())
            return new ActionResult<>(errors);
        return exampleService.update(dto);
    }

    @ApiOperation(value = "获取案例")
    @GetMapping(value = "get/{id}")
    public ActionResult<ExampleDTO> get(@ApiParam("案例id") @PathVariable String id) {
        return exampleService.get(id);
    }

    @ApiOperation("升序")
    @PutMapping("/up/{id}")
    public ActionResult<String> up(@ApiParam("id") @PathVariable String id) {
        return exampleService.up(id);
    }

    @ApiOperation("降序")
    @PutMapping("/down/{id}")
    public ActionResult<String> down(@ApiParam("id") @PathVariable String id) {
        return exampleService.down(id);
    }

    @ApiOperation(value = "刷新指定案例分类下的案例排序")
    @PutMapping(value = "/refresh/{categoryId}")
    public ActionResult<String> refresh(@ApiParam("案例分类id") @PathVariable String categoryId) {
        return exampleService.refresh(categoryId);
    }

    @ApiOperation("查询指定条件的案例")
    @GetMapping("/pageQuery")
    public ActionResult<Pagination<ExampleDTO>> pageQuery(@ApiParam("案例查询DTO") @ModelAttribute ExampleQueryDTO queryDTO){
        //验证页码
        if (queryDTO.getPageNo() == null || queryDTO.getPageNo() <= 0)
            queryDTO.setPageNo(1);
        if (queryDTO.getPageSize() == null || queryDTO.getPageSize() <= 0)
            queryDTO.setPageSize(10);
        return exampleService.listByQueryDTO(queryDTO);
    }
}
