package com.fuxiang.web.service;


import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dto.ProductCategoryDTO;
import com.fuxiang.web.common.Pagination;

import java.util.List;

public interface ProductCategoryService {
    ActionResult<String> add(ProductCategoryDTO productCategoryDTO);

    ActionResult<String> delete(String id);

    ActionResult<String> update(ProductCategoryDTO productCategory);

    ActionResult<ProductCategoryDTO> get(String id);

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);

    ActionResult<List<ProductCategoryDTO>> list(String parentId);

    ActionResult<Pagination<ProductCategoryDTO>> page(String parentId, Integer pageNo, Integer pageSize);

    ActionResult<String> refresh(String id);

    ActionResult<List<ProductCategoryDTO>> listAll();
}
