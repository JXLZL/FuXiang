package com.fuxiang.web.service;


import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.NewsCategoryDTO;

import java.util.List;

public interface NewsCategoryService {
    ActionResult<String> add(NewsCategoryDTO newsCategoryDTO);

    ActionResult<String> delete(String id);

    ActionResult<String> update(NewsCategoryDTO productCategory);

    ActionResult<NewsCategoryDTO> get(String id);

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);

    ActionResult<List<NewsCategoryDTO>> list(String parentId);

    ActionResult<Pagination<NewsCategoryDTO>> page(String parentId, Integer pageNo, Integer pageSize);

    ActionResult<String> refresh(String id);

    ActionResult<List<NewsCategoryDTO>> listAll();
}
