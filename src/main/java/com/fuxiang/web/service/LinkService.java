package com.fuxiang.web.service;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dto.LinkDTO;

import java.util.List;

public interface LinkService {
    ActionResult<String> add(LinkDTO linkDTO);

    ActionResult<String> delete(String id);

    ActionResult<String> update(LinkDTO linkDTO);

    ActionResult<LinkDTO> get(String id);

    ActionResult<List<LinkDTO>> list();

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<List<String>> batchUpdate(List<LinkDTO> linkDTOList);

    ActionResult refresh();

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);
}
