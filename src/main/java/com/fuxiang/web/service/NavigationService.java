package com.fuxiang.web.service;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dto.NavigationDTO;

public interface NavigationService {
    ActionResult<NavigationDTO> get();
}
