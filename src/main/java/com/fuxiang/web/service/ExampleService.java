package com.fuxiang.web.service;


import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.ExampleDTO;
import com.fuxiang.web.dto.query.ExampleQueryDTO;

import java.util.List;

public interface ExampleService {
    ActionResult<String> add(ExampleDTO exampleDTO);

    ActionResult<String> delete(String id);

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<String> update(ExampleDTO exampleDTO);

    ActionResult<ExampleDTO> get(String id);

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);

    ActionResult<String> refresh(String categoryId);

    ActionResult<Pagination<ExampleDTO>> listByQueryDTO(ExampleQueryDTO queryDTO);

}
