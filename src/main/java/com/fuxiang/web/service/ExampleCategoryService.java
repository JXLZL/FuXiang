package com.fuxiang.web.service;


import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.ExampleCategoryDTO;

import java.util.List;

public interface ExampleCategoryService {
    ActionResult<String> add(ExampleCategoryDTO exampleCategoryDTO);

    ActionResult<String> delete(String id);

    ActionResult<String> update(ExampleCategoryDTO productCategory);

    ActionResult<ExampleCategoryDTO> get(String id);

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);

    ActionResult<List<ExampleCategoryDTO>> list(String parentId);

    ActionResult<Pagination<ExampleCategoryDTO>> page(String parentId, Integer pageNo, Integer pageSize);

    ActionResult<String> refresh(String id);

    ActionResult<List<ExampleCategoryDTO>> listAll();

}
