package com.fuxiang.web.service;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.ImageDTO;
import org.springframework.web.multipart.MultipartFile;

public interface ImageService {
    ActionResult<ImageDTO> upload(MultipartFile file, String module);

    ActionResult<String> delete(String id);

    ActionResult<String> collect(String id);

    ActionResult<String> ref(String id);

    ActionResult<ImageDTO> get(String id);

    ActionResult<Pagination<ImageDTO>> pageQuery(String module, Boolean collected, int pageNo, int pageSize);
}
