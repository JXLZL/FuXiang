package com.fuxiang.web.service;


import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.ProductDTO;
import com.fuxiang.web.dto.query.ProductQueryDTO;

import java.util.List;

public interface ProductService {
    ActionResult<String> add(ProductDTO dto);

    ActionResult<String> delete(String id);

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<String> update(ProductDTO dto);

    ActionResult<ProductDTO> get(String id);

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);

    ActionResult<String> refresh(String categoryId);

    ActionResult<Pagination<ProductDTO>> listByQueryDTO(ProductQueryDTO queryDTO);

}
