package com.fuxiang.web.service;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.MessageDTO;
import com.fuxiang.web.dto.query.MessageQueryDTO;

import java.util.List;

public interface MessageService {
    ActionResult<String> add(MessageDTO dto);

    ActionResult<String> delete(String id);

    //不能修改
    //ActionResult<String> update(MessageDTO dto);

    ActionResult<MessageDTO> get(String id);

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<Pagination<MessageDTO>> listByQueryDTO(MessageQueryDTO queryDTO);

    ActionResult<String> deal(String id);

    ActionResult<List<String>> batchDeal(List<String> ids);

}
