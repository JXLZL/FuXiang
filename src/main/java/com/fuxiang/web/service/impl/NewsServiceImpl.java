package com.fuxiang.web.service.impl;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.SystemContext;
import com.fuxiang.web.dao.NewsDao;
import com.fuxiang.web.entity.News;
import com.fuxiang.web.entity.NewsCategory;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dao.NewsCategoryDao;
import com.fuxiang.web.dto.NewsDTO;
import com.fuxiang.web.dto.query.NewsQueryDTO;
import com.fuxiang.web.service.NewsService;
import com.fuxiang.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class NewsServiceImpl implements NewsService {
    @Autowired
    private NewsDao newsDao;

    @Autowired
    private NewsCategoryDao newsCategoryDao;

    @Override
    public ActionResult<String> add(NewsDTO dto) {
        News news = EntityUtils.copyObjectWithInit(dto, News.class);
        Integer max = newsDao.getMaxSortIndex(news.getCategoryId());
        max = max == null ? 0 : max + 1;
        news.setSortIndex(max);
        //region 验证分类是否存在和是否叶子节点，无分类即categoryId=root跳过
        if (!"root".equals(dto.getCategoryId())) {
            NewsCategory newsCategory = newsCategoryDao.get(dto.getCategoryId());
            if (newsCategory == null)
                return new ActionResult<>(-1, "新闻分类不存在，categoryId=" + dto.getCategoryId());
            if (!newsCategory.getLeaf())
                return new ActionResult<>(-1, "新闻分类不是叶子节点，categoryId=" + dto.getCategoryId());
        }
        //endregion
        newsDao.insert(news);
        return new ActionResult<>(news.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        newsDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        newsDao.batchDelete(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<String> update(NewsDTO dto) {
        News po = newsDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "修改失败，不存在id=" + dto.getId() + "的数据");
        //region 验证分类是否存在和是否叶子节点，无分类即categoryId=root跳过
        if (!po.getCategoryId().equals(dto.getCategoryId())) {
            if (!"root".equals(dto.getCategoryId())) {
                NewsCategory newsCategory = newsCategoryDao.get(dto.getCategoryId());
                if (newsCategory == null)
                    return new ActionResult<>(-1, "新闻分类不存在，categoryId=" + dto.getCategoryId());
                if (!newsCategory.getLeaf())
                    return new ActionResult<>(-1, "新闻分类不是叶子节点，categoryId=" + dto.getCategoryId());
                Integer max = newsDao.getMaxSortIndex(dto.getCategoryId());
                max = max == null ? 0 : max + 1;
                po.setSortIndex(max);
            }
        }
        //endregion
        po.setCategoryId(dto.getCategoryId());
        po.setUpdateId(SystemContext.getUserId());
        po.setName(dto.getName());
        po.setIntro(dto.getIntro());
        po.setImageUrl(dto.getImageUrl());
        po.setBigImageUrl(dto.getBigImageUrl());
        po.setRecommend(dto.getRecommend());
        po.setSeoTitle(dto.getSeoTitle());
        po.setSeoKey(dto.getSeoKey());
        po.setSeoDescription(dto.getSeoDescription());
        po.setContent(dto.getContent());
        po.setComment(dto.getComment());
        newsDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<NewsDTO> get(String id) {
        News po = newsDao.get(id);
        NewsDTO dto = EntityUtils.copyObject(po, NewsDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<String> up(String id) {
        News po = newsDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "升序失败，不存在id=" + id + "的数据");
        List<News> poList = newsDao.listAllByCategoryId(po.getCategoryId());
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "升序失败，不存在同级数据");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != 0) {
                poList.set(i, poList.get(i - 1));
                poList.set(i - 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        newsDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> down(String id) {
        News po = newsDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "降序失败，不存在id=" + id + "的数据");
        List<News> poList = newsDao.listAllByCategoryId(po.getCategoryId());
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "降序失败，不存在同级数据");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != (poList.size() - 1)) {
                poList.set(i, poList.get(i + 1));
                poList.set(i + 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        newsDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> refresh(String categoryId) {
        List<News> poList = newsDao.listAllByCategoryId(categoryId);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "刷新新闻排序失败，不存在该新闻分类下的数据");
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        newsDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(categoryId);
    }

    @Override
    public ActionResult<Pagination<NewsDTO>> listByQueryDTO(NewsQueryDTO queryDTO) {
        Pagination<NewsDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = newsDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<News> poList = newsDao.listByQueryDTO(queryDTO);
            List<NewsDTO> dtoList = EntityUtils.copyList(poList, NewsDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }
}
