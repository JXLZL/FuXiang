package com.fuxiang.web.service.impl;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dao.ExampleCategoryDao;
import com.fuxiang.web.dao.ProductCategoryDao;
import com.fuxiang.web.dto.ExampleCategoryDTO;
import com.fuxiang.web.dto.NavigationDTO;
import com.fuxiang.web.dto.NewsCategoryDTO;
import com.fuxiang.web.dto.ProductCategoryDTO;
import com.fuxiang.web.entity.ExampleCategory;
import com.fuxiang.web.entity.NewsCategory;
import com.fuxiang.web.entity.ProductCategory;
import com.fuxiang.web.service.NavigationService;
import com.fuxiang.web.util.EntityUtils;
import com.fuxiang.web.dao.NewsCategoryDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NavigationServiceImpl implements NavigationService {
    @Autowired
    private NewsCategoryDao newsCategoryDao;

    @Autowired
    private ProductCategoryDao productCategoryDao;

    @Autowired
    private ExampleCategoryDao exampleCategoryDao;

    @Override
    public ActionResult<NavigationDTO> get() {
        NavigationDTO dto = new NavigationDTO();
        List<NewsCategory> newsCategoryList = newsCategoryDao.listAll();
        dto.setNewsCategoryDTOS(EntityUtils.copyList(newsCategoryList, NewsCategoryDTO.class));
        List<ProductCategory> productCategoryList = productCategoryDao.listAll();
        dto.setProductCategoryDTOS(EntityUtils.copyList(productCategoryList, ProductCategoryDTO.class));
        List<ExampleCategory> exampleCategoryList = exampleCategoryDao.listAll();
        dto.setExampleCategoryDTOS(EntityUtils.copyList(exampleCategoryList, ExampleCategoryDTO.class));
        return new ActionResult<>(dto);
    }
}
