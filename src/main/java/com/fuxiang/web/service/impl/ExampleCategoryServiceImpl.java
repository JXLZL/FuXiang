package com.fuxiang.web.service.impl;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dao.ExampleCategoryDao;
import com.fuxiang.web.dto.ExampleCategoryDTO;
import com.fuxiang.web.enums.Operation;
import com.fuxiang.web.service.ExampleCategoryService;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dao.ExampleDao;
import com.fuxiang.web.entity.ExampleCategory;
import com.fuxiang.web.util.EntityUtils;
import com.fuxiang.web.util.RstUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class ExampleCategoryServiceImpl implements ExampleCategoryService {
    @Autowired
    private ExampleCategoryDao exampleCategoryDao;

    @Autowired
    private ExampleDao exampleDao;

    @Override
    public ActionResult<String> add(ExampleCategoryDTO dto) {

        ExampleCategory po = EntityUtils.copyObjectWithInit(dto, ExampleCategory.class);

        //如果有父节点，则验证父节点是否存在
        if (!"root".equals(po.getParentId())) {
            ExampleCategory parent = exampleCategoryDao.get(po.getParentId());
            if (parent == null)
                return new ActionResult<>(-1, "不存在id=" + po.getParentId() + "的父节点");
            if (parent.getLeaf())
                return new ActionResult<>(-1, "id=" + parent.getId() + "的节点是叶子节点无法添加子分类");
            po.setLevel(parent.getLevel() + 1);
        } else {
            po.setLevel(0);
        }
        //获取最大排序
        Integer max = exampleCategoryDao.getMaxSortIndex(po.getParentId());
        max = max == null ? 0 : max + 1;
        po.setSortIndex(max);

        exampleCategoryDao.insert(po);
        return new ActionResult<>(po.getId());
    }

    /**
     * 删除该产品类别,如果该类别下有子类别或产品则禁止删除
     */
    @Override
    public ActionResult<String> delete(String id) {
        ExampleCategory po = exampleCategoryDao.get(id);
        RstUtils.nullError(po, id, Operation.FAIL_DEL);
        if (po.getLeaf()) {
            //判断是否有产品
        } else {
            //判断是否有子类别
            int count = exampleCategoryDao.countByParentId(id);
            if (count > 0)
                return new ActionResult<>(-1, "删除失败，该分类下有子分类");
        }

        String updateId = "更新者id";
        exampleCategoryDao.delete(id, updateId);
        return new ActionResult<>(id);
    }

    /**
     * 允许修改的属性：
     */
    @Override
    public ActionResult<String> update(ExampleCategoryDTO dto) {
        //判断数据是否存在
        ExampleCategory po = exampleCategoryDao.get(dto.getId());
        RstUtils.nullError(po, dto.getId(), Operation.FAIL_UPD);

        if (!dto.getParentId().equals(po.getParentId())) {
            if (!"root".equals(dto.getParentId())) {
                //验证父节点是否存在
                ExampleCategory parent = exampleCategoryDao.get(dto.getParentId());
                if (parent == null)
                    return new ActionResult<>(-1, "修改失败，id=" + dto.getParentId() + "的数据不存在");
                if (parent.getLeaf())
                    return new ActionResult<>(-1, "修改失败，id=" + dto.getParentId() + "的数据为叶子节点");
                po.setLevel(parent.getLevel() + 1);
            } else {
                po.setLevel(0);
            }
            Integer max = exampleCategoryDao.getMaxSortIndex(dto.getParentId());
            max = max == null ? 0 : max + 1;
            po.setSortIndex(max);
        }

        po.setName(dto.getName());
        po.setIntro(dto.getIntro());
        po.setImageUrl(dto.getImageUrl());
        po.setSeoTitle(dto.getSeoTitle());
        po.setSeoKey(dto.getSeoKey());
        po.setSeoDescription(dto.getSeoDescription());
        EntityUtils.update(po);
        exampleCategoryDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<ExampleCategoryDTO> get(String id) {
        ExampleCategory po = exampleCategoryDao.get(id);
        RstUtils.nullError(po, id, Operation.FAIL_GET);
        ExampleCategoryDTO dto = EntityUtils.copyObject(po, ExampleCategoryDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<String> up(String id) {
        ExampleCategory po = exampleCategoryDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "升序失败，不存在id=" + id + "的数据");
        List<ExampleCategory> poList = exampleCategoryDao.listByParentId(po.getParentId(), null, null);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "升序失败，不存在同级分类");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != 0) {
                poList.set(i, poList.get(i - 1));
                poList.set(i - 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        String updateId = "更新者id";
        exampleCategoryDao.batchUpdateSortIndex(poList, updateId);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> down(String id) {
        ExampleCategory po = exampleCategoryDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "降序失败，不存在id=" + id + "的数据");
        List<ExampleCategory> poList = exampleCategoryDao.listByParentId(po.getParentId(), null, null);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "降序失败，不存在同级分类");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != (poList.size() - 1)) {
                poList.set(i, poList.get(i + 1));
                poList.set(i + 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        String updateId = "更新者id";
        exampleCategoryDao.batchUpdateSortIndex(poList, updateId);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<ExampleCategoryDTO>> list(String parentId) {
        List<ExampleCategory> poList = exampleCategoryDao.listByParentId(parentId, null, null);
        List<ExampleCategoryDTO> dtoList = EntityUtils.copyList(poList, ExampleCategoryDTO.class);
        return new ActionResult<>(dtoList);
    }

    @Override
    public ActionResult<Pagination<ExampleCategoryDTO>> page(String parentId, Integer pageNo, Integer pageSize) {
        Pagination<ExampleCategoryDTO> pagination = new Pagination<>(pageNo, pageSize);
        if (exampleCategoryDao.countByParentId(parentId) > 0) {
            List<ExampleCategory> poList = exampleCategoryDao.listByParentId(parentId, pagination.getBeginIndex(), pageSize);
            pagination.setRows(EntityUtils.copyList(poList, ExampleCategoryDTO.class));
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> refresh(String id) {
        List<ExampleCategory> poList = exampleCategoryDao.listByParentId(id, null, null);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "刷新失败，不存在父节点id=" + id + "的子分类");
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        String updateId = "更新者id";
        exampleCategoryDao.batchUpdateSortIndex(poList, updateId);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<ExampleCategoryDTO>> listAll() {
        List<ExampleCategory> poList = exampleCategoryDao.listAll();
        return new ActionResult<>(EntityUtils.copyList(poList, ExampleCategoryDTO.class));
    }

}
