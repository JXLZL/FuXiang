package com.fuxiang.web.service.impl;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dao.NewsDao;
import com.fuxiang.web.dto.NewsCategoryDTO;
import com.fuxiang.web.entity.NewsCategory;
import com.fuxiang.web.enums.Operation;
import com.fuxiang.web.service.NewsCategoryService;
import com.fuxiang.web.util.EntityUtils;
import com.fuxiang.web.dao.NewsCategoryDao;
import com.fuxiang.web.util.RstUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class NewsCategoryServiceImpl implements NewsCategoryService {
    @Autowired
    private NewsCategoryDao newsCategoryDao;

    @Autowired
    private NewsDao newsDao;

    @Override
    public ActionResult<String> add(NewsCategoryDTO dto) {

        NewsCategory po = EntityUtils.copyObjectWithInit(dto, NewsCategory.class);

        //如果有父节点，则验证父节点是否存在
        if (!"root".equals(po.getParentId())) {
            NewsCategory parent = newsCategoryDao.get(po.getParentId());
            if (parent == null)
                return new ActionResult<>(-1, "不存在id=" + po.getParentId() + "的父节点");
            if (parent.getLeaf())
                return new ActionResult<>(-1, "id=" + parent.getId() + "的节点是叶子节点无法添加子分类");
            po.setLevel(parent.getLevel() + 1);
        } else {
            po.setLevel(0);
        }
        //获取最大排序
        Integer max = newsCategoryDao.getMaxSortIndex(po.getParentId());
        max = max == null ? 0 : max + 1;
        po.setSortIndex(max);

        newsCategoryDao.insert(po);
        return new ActionResult<>(po.getId());
    }

    /**
     * 删除该产品类别,如果该类别下有子类别或产品则禁止删除
     */
    @Override
    public ActionResult<String> delete(String id) {
        NewsCategory po = newsCategoryDao.get(id);
        RstUtils.nullError(po, id, Operation.FAIL_DEL);
        if (po.getLeaf()) {
            //判断是否有产品
        } else {
            //判断是否有子类别
            int count = newsCategoryDao.countByParentId(id);
            if (count > 0)
                return new ActionResult<>(-1, "删除失败，该分类下有子分类");
        }

        String updateId = "更新者id";
        newsCategoryDao.delete(id, updateId);
        return new ActionResult<>(id);
    }

    /**
     * 允许修改的属性：
     */
    @Override
    public ActionResult<String> update(NewsCategoryDTO dto) {
        //判断数据是否存在
        NewsCategory po = newsCategoryDao.get(dto.getId());
        RstUtils.nullError(po, dto.getId(), Operation.FAIL_UPD);

        if (!dto.getParentId().equals(po.getParentId())) {
            if (!"root".equals(dto.getParentId())) {
                //验证父节点是否存在
                NewsCategory parent = newsCategoryDao.get(dto.getParentId());
                if (parent == null)
                    return new ActionResult<>(-1, "修改失败，id=" + dto.getParentId() + "的数据不存在");
                if (parent.getLeaf())
                    return new ActionResult<>(-1, "修改失败，id=" + dto.getParentId() + "的数据为叶子节点");
                po.setLevel(parent.getLevel() + 1);
            } else {
                po.setLevel(0);
            }
            Integer max = newsCategoryDao.getMaxSortIndex(dto.getParentId());
            max = max == null ? 0 : max + 1;
            po.setSortIndex(max);
        }

        po.setName(dto.getName());
        po.setIntro(dto.getIntro());
        po.setImageUrl(dto.getImageUrl());
        po.setSeoTitle(dto.getSeoTitle());
        po.setSeoKey(dto.getSeoKey());
        po.setSeoDescription(dto.getSeoDescription());
        EntityUtils.update(po);
        newsCategoryDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<NewsCategoryDTO> get(String id) {
        NewsCategory po = newsCategoryDao.get(id);
        RstUtils.nullError(po, id, Operation.FAIL_GET);
        NewsCategoryDTO dto = EntityUtils.copyObject(po, NewsCategoryDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<String> up(String id) {
        NewsCategory po = newsCategoryDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "升序失败，不存在id=" + id + "的数据");
        List<NewsCategory> poList = newsCategoryDao.listByParentId(po.getParentId(), null, null);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "升序失败，不存在同级分类");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != 0) {
                poList.set(i, poList.get(i - 1));
                poList.set(i - 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        String updateId = "更新者id";
        newsCategoryDao.batchUpdateSortIndex(poList, updateId);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> down(String id) {
        NewsCategory po = newsCategoryDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "降序失败，不存在id=" + id + "的数据");
        List<NewsCategory> poList = newsCategoryDao.listByParentId(po.getParentId(), null, null);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "降序失败，不存在同级分类");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != (poList.size() - 1)) {
                poList.set(i, poList.get(i + 1));
                poList.set(i + 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        String updateId = "更新者id";
        newsCategoryDao.batchUpdateSortIndex(poList, updateId);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<NewsCategoryDTO>> list(String parentId) {
        List<NewsCategory> poList = newsCategoryDao.listByParentId(parentId, null, null);
        List<NewsCategoryDTO> dtoList = EntityUtils.copyList(poList, NewsCategoryDTO.class);
        return new ActionResult<>(dtoList);
    }

    @Override
    public ActionResult<Pagination<NewsCategoryDTO>> page(String parentId, Integer pageNo, Integer pageSize) {
        Pagination<NewsCategoryDTO> pagination = new Pagination<>(pageNo, pageSize);
        if (newsCategoryDao.countByParentId(parentId) > 0) {
            List<NewsCategory> poList = newsCategoryDao.listByParentId(parentId, pagination.getBeginIndex(), pageSize);
            pagination.setRows(EntityUtils.copyList(poList, NewsCategoryDTO.class));
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> refresh(String id) {
        List<NewsCategory> poList = newsCategoryDao.listByParentId(id, null, null);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "刷新失败，不存在父节点id=" + id + "的子分类");
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        String updateId = "更新者id";
        newsCategoryDao.batchUpdateSortIndex(poList, updateId);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<NewsCategoryDTO>> listAll() {
        List<NewsCategory> poList = newsCategoryDao.listAll();
        return new ActionResult<>(EntityUtils.copyList(poList, NewsCategoryDTO.class));
    }
}
