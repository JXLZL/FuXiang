package com.fuxiang.web.service.impl;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.SystemContext;
import com.fuxiang.web.dao.ProductCategoryDao;
import com.fuxiang.web.dto.query.ProductQueryDTO;
import com.fuxiang.web.entity.Product;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dao.ProductDao;
import com.fuxiang.web.dto.ProductDTO;
import com.fuxiang.web.entity.ProductCategory;
import com.fuxiang.web.service.ProductService;
import com.fuxiang.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class ProductServiceImpl implements ProductService {
    @Autowired
    private ProductDao productDao;

    @Autowired
    private ProductCategoryDao productCategoryDao;

    @Override
    public ActionResult<String> add(ProductDTO dto) {
        Product po = EntityUtils.copyObjectWithInit(dto, Product.class);
        Integer max = productDao.getMaxSortIndex(po.getCategoryId());
        max = max == null ? 0 : max + 1;
        po.setSortIndex(max);
        //region 验证分类是否存在和是否叶子节点，无分类即categoryId=root跳过
        if (!"root".equals(dto.getCategoryId())) {
            ProductCategory productCategory = productCategoryDao.get(dto.getCategoryId());
            if (productCategory == null)
                return new ActionResult<>(-1, "产品分类不存在，categoryId=" + dto.getCategoryId());
            if (!productCategory.getLeaf())
                return new ActionResult<>(-1, "产品分类不是叶子节点，categoryId=" + dto.getCategoryId());
        }
        //endregion
        productDao.insert(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        productDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        productDao.batchDelete(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<String> update(ProductDTO dto) {
        Product po = productDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "修改失败，不存在id=" + dto.getId() + "的数据");
        //region 验证分类是否存在和是否叶子节点，无分类即categoryId=root跳过
        if (!po.getCategoryId().equals(dto.getCategoryId())) {
            if (!"root".equals(dto.getCategoryId())) {
                ProductCategory productCategory = productCategoryDao.get(dto.getCategoryId());
                if (productCategory == null)
                    return new ActionResult<>(-1, "产品分类不存在，categoryId=" + dto.getCategoryId());
                if (!productCategory.getLeaf())
                    return new ActionResult<>(-1, "产品分类不是叶子节点，categoryId=" + dto.getCategoryId());
                Integer max = productDao.getMaxSortIndex(dto.getCategoryId());
                max = max == null ? 0 : max + 1;
                po.setSortIndex(max);
            }
        }
        //endregion
        po.setCategoryId(dto.getCategoryId());
        po.setUpdateId(SystemContext.getUserId());
        po.setName(dto.getName());
        po.setIntro(dto.getIntro());
        po.setImageUrl(dto.getImageUrl());
        po.setBigImageUrl(dto.getBigImageUrl());
        po.setRecommend(dto.getRecommend());
        po.setSeoTitle(dto.getSeoTitle());
        po.setSeoKey(dto.getSeoKey());
        po.setSeoDescription(dto.getSeoDescription());
        po.setContent(dto.getContent());
        po.setComment(dto.getComment());
        productDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<ProductDTO> get(String id) {
        Product po = productDao.get(id);
        ProductDTO dto = EntityUtils.copyObject(po, ProductDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<String> up(String id) {
        Product po = productDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "升序失败，不存在id=" + id + "的数据");
        List<Product> poList = productDao.listAllByCategoryId(po.getCategoryId());
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "升序失败，不存在同级数据");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != 0) {
                poList.set(i, poList.get(i - 1));
                poList.set(i - 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        productDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> down(String id) {
        Product po = productDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "降序失败，不存在id=" + id + "的数据");
        List<Product> poList = productDao.listAllByCategoryId(po.getCategoryId());
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "降序失败，不存在同级数据");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != (poList.size() - 1)) {
                poList.set(i, poList.get(i + 1));
                poList.set(i + 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        productDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> refresh(String categoryId) {
        List<Product> poList = productDao.listAllByCategoryId(categoryId);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "刷新排序失败，不存在该分类的数据");
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        productDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(categoryId);
    }

    @Override
    public ActionResult<Pagination<ProductDTO>> listByQueryDTO(ProductQueryDTO queryDTO) {
        int count = productDao.countByQueryDTO(queryDTO);
        List<Product> poList = productDao.listByQueryDTO(queryDTO);
        List<ProductDTO> dtoList = EntityUtils.copyList(poList, ProductDTO.class);
        Pagination<ProductDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        pagination.setCount(count);
        pagination.setRows(dtoList);
        return new ActionResult<>(pagination);
    }
}
