package com.fuxiang.web.service.impl;

import com.fuxiang.web.dto.LinkDTO;
import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.BusinessException;
import com.fuxiang.web.dao.LinkDao;
import com.fuxiang.web.entity.Link;
import com.fuxiang.web.service.LinkService;
import com.fuxiang.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class LinkServiceImpl implements LinkService {
    @Autowired
    private LinkDao linkDao;

    @Override
    public ActionResult<String> add(LinkDTO linkDTO) {
        Link link = EntityUtils.copyObjectWithInit(linkDTO, Link.class);
        //查找已有数据中排序个最大的
        Integer maxSortIndex = linkDao.getMaxSortIndex();
        link.setSortIndex(maxSortIndex == null ? 0 : maxSortIndex + 1);
        linkDao.insert(link);
        return new ActionResult<>(link.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        Link link = linkDao.get(id);
        if (link == null)
            return new ActionResult<>(id);
        String updateId = "更新者id";
        linkDao.delete(id, updateId);
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> update(LinkDTO linkDTO) {
        Link link = linkDao.get(linkDTO.getId());
        if (link == null)
            throw new BusinessException(-1, "更新失败，友情链接数据不存在");
        link.setName(linkDTO.getName());
        link.setUrl(linkDTO.getUrl());
        link.setComment(linkDTO.getComment());
        EntityUtils.update(link);
        linkDao.update(link);
        return new ActionResult<>(link.getId());
    }

    @Override
    public ActionResult<LinkDTO> get(String id) {
        Link link = linkDao.get(id);
        LinkDTO linkDTO = EntityUtils.copyObject(link, LinkDTO.class);
        return new ActionResult<>(linkDTO);
    }

    @Override
    public ActionResult<List<LinkDTO>> list() {

        List<Link> linkList = linkDao.list();
        List<LinkDTO> linkDTOList = EntityUtils.copyList(linkList, LinkDTO.class);
        return new ActionResult<>(linkDTOList);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        if (CollectionUtils.isEmpty(ids))
            return new ActionResult<>(ids);
        String updateId = "更新者id";
        linkDao.batchDelete(ids, updateId);
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<List<String>> batchUpdate(List<LinkDTO> linkDTOList) {
        return null;
    }

    @Override
    public ActionResult refresh() {
        List<Link> linkList = linkDao.list();
        //重新设置排序
        for (int i = 0; i < linkList.size(); i++) {
            linkList.get(i).setSortIndex(i);
            EntityUtils.update(linkList.get(i));
        }

        //批量修改排序
        String updateId = "更新者id";
        linkDao.batchUpdateSortIndex(linkList, updateId);
        return new ActionResult(true, null, null);
    }

    @Override
    public ActionResult<String> up(String id) {
        Link link = linkDao.get(id);
        if (link == null)
            throw new BusinessException(-1, "升序失败，不存在id=" + id + "的数据");
        if (link.getSortIndex() == 0)
            return new ActionResult<>(id);
        List<Link> linkList = linkDao.list();
        for (int i = 0; i < linkList.size(); i++) {
            if (id.equals(linkList.get(i).getId()) && i != 0) {
                linkList.set(i, linkList.get(i - 1));
                linkList.set(i - 1, link);
                break;
            }
        }

        //重新设置排序
        for (int i = 0; i < linkList.size(); i++) {
            linkList.get(i).setSortIndex(i);
            EntityUtils.update(linkList.get(i));
        }

        //批量修改排序
        String updateId = "更新者id";
        linkDao.batchUpdateSortIndex(linkList, updateId);

        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> down(String id) {

        Link link = linkDao.get(id);
        if (link == null)
            throw new BusinessException(-1, "降序失败，不存在id=" + id + "的数据");
        List<Link> linkList = linkDao.list();
        for (int i = 0; i < linkList.size(); i++) {
            if (id.equals(linkList.get(i).getId()) && i != (linkList.size() - 1)) {
                linkList.set(i, linkList.get(i + 1));
                linkList.set(i + 1, link);
                break;
            }
        }

        //重新设置排序
        for (int i = 0; i < linkList.size(); i++) {
            linkList.get(i).setSortIndex(i);
            EntityUtils.update(linkList.get(i));
        }

        //批量修改排序
        String updateId = "更新者id";
        linkDao.batchUpdateSortIndex(linkList, updateId);

        return new ActionResult<>(id);
    }
}
