package com.fuxiang.web.service.impl;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dao.ImageDao;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.common.SystemContext;
import com.fuxiang.web.dto.ImageDTO;
import com.fuxiang.web.entity.Image;
import com.fuxiang.web.service.ImageService;
import com.fuxiang.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.List;

@Service
public class ImageServiceImpl implements ImageService {

    @Value("${upload.image.path}")
    private String imagePath;

    @Value("${upload.image.dir}")
    private String imageDir;

    @Value("${upload.image.trash}")
    private String imageTrashDir;

    @Autowired
    private ImageDao imageDao;

    @Override
    public ActionResult<ImageDTO> upload(MultipartFile file, String module) {
        String filename = file.getOriginalFilename();
        String suffix = filename.substring(filename.lastIndexOf('.'));
        Image po = EntityUtils.init(Image.class);
        String imageName = po.getId() + suffix;
        po.setName(filename);
        po.setUrl(imagePath + imageName);
        po.setRealPath(imageDir + imageName);
        po.setModule(module);
        po.setRefCount(1);
        po.setCollected(false);

        File dest = new File(po.getRealPath());
        //判断目录是否存在
        if (!dest.getParentFile().exists())
            dest.getParentFile().mkdirs();
        try {
            file.transferTo(dest);
        } catch (IOException e) {
            System.out.println("保存图片异常");
            e.printStackTrace();
        }
        imageDao.insert(po);
        return new ActionResult<>(EntityUtils.copyObject(po, ImageDTO.class));
    }

    @Override
    public ActionResult<String> delete(String id) {
        Image po = imageDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "删除失败，不存在id=" + id + "的数据");

        String destPath = imageTrashDir + po.getId() + po.getName().substring(po.getName().lastIndexOf('.'));
        File dest = new File(destPath);
        if (!dest.getParentFile().exists())
            dest.getParentFile().mkdirs();
        File file = new File(po.getRealPath());
        if (file.exists()) {
            if (file.equals(dest)) {
                file.delete();
            } else {
                file.renameTo(dest);
            }
        }
        imageDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> collect(String id) {
        Image po = imageDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "收藏失败，不存在id=" + id + "的数据");
        imageDao.collect(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> ref(String id) {
        Image po = imageDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "引用失败，不存在id=" + id + "的数据");
        imageDao.ref(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<ImageDTO> get(String id) {
        return new ActionResult<>(EntityUtils.copyObject(imageDao.get(id), ImageDTO.class));
    }

    @Override
    public ActionResult<Pagination<ImageDTO>> pageQuery(String module, Boolean collected, int pageNo, int pageSize) {
        Pagination<ImageDTO> pagination = new Pagination<>(pageNo, pageSize);
        int count = imageDao.countByModuleOrCollected(module, collected);
        pagination.setCount(count);
        if (count > 0) {
            List<Image> poList = imageDao.listByModuleOrCollected(module, collected, pagination.getBeginIndex(), pagination.getPageSize());
            pagination.setRows(EntityUtils.copyList(poList, ImageDTO.class));
        }
        return new ActionResult<>(pagination);
    }
}
