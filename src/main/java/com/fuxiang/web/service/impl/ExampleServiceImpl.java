package com.fuxiang.web.service.impl;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.SystemContext;
import com.fuxiang.web.dao.ExampleCategoryDao;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dao.ExampleDao;
import com.fuxiang.web.dto.ExampleDTO;
import com.fuxiang.web.dto.query.ExampleQueryDTO;
import com.fuxiang.web.entity.Example;
import com.fuxiang.web.entity.ExampleCategory;
import com.fuxiang.web.service.ExampleService;
import com.fuxiang.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.List;

@Service
public class ExampleServiceImpl implements ExampleService {
    @Autowired
    private ExampleDao exampleDao;

    @Autowired
    private ExampleCategoryDao exampleCategoryDao;

    @Override
    public ActionResult<String> add(ExampleDTO dto) {
        Example po = EntityUtils.copyObjectWithInit(dto, Example.class);
        Integer max = exampleDao.getMaxSortIndex(po.getCategoryId());
        max = max == null ? 0 : max + 1;
        po.setSortIndex(max);
        //region 验证分类是否存在和是否叶子节点，无分类即categoryId=root跳过
        if (!"root".equals(dto.getCategoryId())) {
            ExampleCategory exampleCategory = exampleCategoryDao.get(dto.getCategoryId());
            if (exampleCategory == null)
                return new ActionResult<>(-1, "案例分类不存在，categoryId=" + dto.getCategoryId());
            if (!exampleCategory.getLeaf())
                return new ActionResult<>(-1, "案例分类不是叶子节点，categoryId=" + dto.getCategoryId());
        }
        //endregion
        exampleDao.insert(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        exampleDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        exampleDao.batchDelete(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<String> update(ExampleDTO dto) {
        Example po = exampleDao.get(dto.getId());
        if (po == null)
            return new ActionResult<>(-1, "修改失败，不存在id=" + dto.getId() + "的数据");
        //region 验证分类是否存在和是否叶子节点，无分类即categoryId=root跳过
        if (!po.getCategoryId().equals(dto.getCategoryId())) {
            if (!"root".equals(dto.getCategoryId())) {
                ExampleCategory exampleCategory = exampleCategoryDao.get(dto.getCategoryId());
                if (exampleCategory == null)
                    return new ActionResult<>(-1, "案例分类不存在，categoryId=" + dto.getCategoryId());
                if (!exampleCategory.getLeaf())
                    return new ActionResult<>(-1, "案例分类不是叶子节点，categoryId=" + dto.getCategoryId());
                Integer max = exampleDao.getMaxSortIndex(dto.getCategoryId());
                max = max == null ? 0 : max + 1;
                po.setSortIndex(max);
            }
        }
        //endregion
        po.setCategoryId(dto.getCategoryId());
        po.setUpdateId(SystemContext.getUserId());
        po.setName(dto.getName());
        po.setIntro(dto.getIntro());
        po.setImageUrl(dto.getImageUrl());
        po.setBigImageUrl(dto.getBigImageUrl());
        po.setRecommend(dto.getRecommend());
        po.setSeoTitle(dto.getSeoTitle());
        po.setSeoKey(dto.getSeoKey());
        po.setSeoDescription(dto.getSeoDescription());
        po.setContent(dto.getContent());
        po.setComment(dto.getComment());
        exampleDao.update(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<ExampleDTO> get(String id) {
        Example po = exampleDao.get(id);
        ExampleDTO dto = EntityUtils.copyObject(po, ExampleDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<String> up(String id) {
        Example po = exampleDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "升序失败，不存在id=" + id + "的数据");
        List<Example> poList = exampleDao.listAllByCategoryId(po.getCategoryId());
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "升序失败，不存在同级数据");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != 0) {
                poList.set(i, poList.get(i - 1));
                poList.set(i - 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        exampleDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> down(String id) {
        Example po = exampleDao.get(id);
        if (po == null)
            return new ActionResult<>(-1, "降序失败，不存在id=" + id + "的数据");
        List<Example> poList = exampleDao.listAllByCategoryId(po.getCategoryId());
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "降序失败，不存在同级数据");
        for (int i = 0; i < poList.size(); i++) {
            if (id.equals(poList.get(i).getId()) && i != (poList.size() - 1)) {
                poList.set(i, poList.get(i + 1));
                poList.set(i + 1, po);
                break;
            }
        }
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        exampleDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<String> refresh(String categoryId) {
        List<Example> poList = exampleDao.listAllByCategoryId(categoryId);
        if (CollectionUtils.isEmpty(poList))
            return new ActionResult<>(-1, "降序失败，不存在同级数据");
        //重新设置排序
        for (int i = 0; i < poList.size(); i++) {
            poList.get(i).setSortIndex(i);
        }
        exampleDao.batchUpdateSortIndex(poList, SystemContext.getUserId());
        return new ActionResult<>(categoryId);
    }

    @Override
    public ActionResult<Pagination<ExampleDTO>> listByQueryDTO(ExampleQueryDTO queryDTO) {
        Pagination<ExampleDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = exampleDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<Example> poList = exampleDao.listByQueryDTO(queryDTO);
            List<ExampleDTO> dtoList = EntityUtils.copyList(poList, ExampleDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }
}
