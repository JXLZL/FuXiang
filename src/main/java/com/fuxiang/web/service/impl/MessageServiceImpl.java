package com.fuxiang.web.service.impl;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.dto.query.MessageQueryDTO;
import com.fuxiang.web.service.MessageService;
import com.fuxiang.web.common.BusinessException;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.common.SystemContext;
import com.fuxiang.web.dao.MessageDao;
import com.fuxiang.web.dto.MessageDTO;
import com.fuxiang.web.entity.Message;
import com.fuxiang.web.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MessageServiceImpl implements MessageService {
    @Autowired
    private MessageDao messageDao;

    @Override
    public ActionResult<String> add(MessageDTO dto) {
        //验证该id是否重复
        String id = dto.getId();
        if (messageDao.get(id) != null)
            return new ActionResult<>(-1, "添加失败，id主键重复，id = " + id);
        Message po = EntityUtils.copyObjectWithInit(dto, Message.class);
        po.setId(id);
        po.setDealed(false);
        po.setDealTime(null);
        messageDao.insert(po);
        return new ActionResult<>(po.getId());
    }

    @Override
    public ActionResult<String> delete(String id) {
        Message message = messageDao.get(id);
        if (message == null)
            return new ActionResult<>(id);
        messageDao.delete(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    //region 不提供修改的接口
    /*@Override
    public ActionResult<String> update(MessageDTO dto) {
        Message po = messageDao.get(dto.getId());
        if (po == null)
            throw new BusinessException(-1, "更新失败，数据不存在，id = " + dto.getId());
        po.setName(dto.getName());
        po.setTel(dto.getTel());
        po.setEmail(dto.getEmail());
        po.setAddress(dto.getAddress());
        po.setZip(dto.getZip());
        po.setSubject(dto.getSubject());
        po.setContent(dto.getContent());
        po.setComment(dto.getComment());
        po.setUpdateId(SystemContext.getUserId());
        messageDao.update(po);
        return new ActionResult<>(po.getId());
    }*/
    //endregion

    @Override
    public ActionResult<MessageDTO> get(String id) {
        Message po = messageDao.get(id);
        MessageDTO dto = EntityUtils.copyObject(po, MessageDTO.class);
        return new ActionResult<>(dto);
    }

    @Override
    public ActionResult<List<String>> batchDelete(List<String> ids) {
        messageDao.batchDelete(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }

    @Override
    public ActionResult<Pagination<MessageDTO>> listByQueryDTO(MessageQueryDTO queryDTO) {
        Pagination<MessageDTO> pagination = new Pagination<>(queryDTO.getPageNo(), queryDTO.getPageSize());
        int count = messageDao.countByQueryDTO(queryDTO);
        pagination.setCount(count);
        if (count > 0) {
            List<Message> poList = messageDao.listByQueryDTO(queryDTO);
            List<MessageDTO> dtoList = EntityUtils.copyList(poList, MessageDTO.class);
            pagination.setRows(dtoList);
        }
        return new ActionResult<>(pagination);
    }

    @Override
    public ActionResult<String> deal(String id) {
        Message po = messageDao.get(id);
        if (po == null)
            throw new BusinessException(-1, "处理留言失败，不存在id=" + id + "的数据");
        messageDao.deal(id, SystemContext.getUserId());
        return new ActionResult<>(id);
    }

    @Override
    public ActionResult<List<String>> batchDeal(List<String> ids) {
        messageDao.batchDeal(ids, SystemContext.getUserId());
        return new ActionResult<>(ids);
    }
}
