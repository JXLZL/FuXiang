package com.fuxiang.web.service;


import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.common.Pagination;
import com.fuxiang.web.dto.NewsDTO;
import com.fuxiang.web.dto.query.NewsQueryDTO;

import java.util.List;

public interface NewsService {
    ActionResult<String> add(NewsDTO dto);

    ActionResult<String> delete(String id);

    ActionResult<List<String>> batchDelete(List<String> ids);

    ActionResult<String> update(NewsDTO dto);

    ActionResult<NewsDTO> get(String id);

    ActionResult<String> up(String id);

    ActionResult<String> down(String id);

    ActionResult<String> refresh(String categoryId);

    ActionResult<Pagination<NewsDTO>> listByQueryDTO(NewsQueryDTO queryDTO);

}
