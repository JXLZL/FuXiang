package com.fuxiang.web.common;

import lombok.Data;

import java.util.Arrays;
import java.util.List;

@Data
public class ActionResult<T> {
    //成功标识
    private Boolean success;

    //返回数据
    private T data;

    //错误列表
    private List<ErrorInfo> errors;

    /**
     * 无参构造方法
     */
    public ActionResult() {
    }

    /**
     * 全参构造方法
     *
     * @param success
     * @param data
     * @param errors
     */
    public ActionResult(Boolean success, T data, List<ErrorInfo> errors) {
        this.success = success;
        this.data = data;
        this.errors = errors;
    }

    /**
     * 成功构造方法
     *
     * @param data
     */
    public ActionResult(T data) {
        this.success = true;
        this.data = data;
    }

    /**
     * 失败构造方法
     *
     * @param errors
     */
    public ActionResult(List<ErrorInfo> errors) {
        this.success = false;
        this.errors = errors;
    }

    /**
     * 失败构造方法
     *
     * @param errors
     */
    public ActionResult(ErrorInfo... errors) {
        this.success = false;
        this.errors = Arrays.asList(errors);
    }

    public ActionResult(Integer code,String message){
        this(new ErrorInfo(code, message));
    }
}

