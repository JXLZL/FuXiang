package com.fuxiang.web.common;

import lombok.Data;

import java.util.List;

@Data
public class Pagination<T> {
    private int pageNo = 1;

    private int pageSize = 10;

    private int beginIndex;

    private int count;

    private List<T> rows;

    public Pagination() {
    }

    public Pagination(int pageNo, int pageSize) {
        this.pageNo = pageNo;
        this.pageSize = pageSize;
    }

    public int getBeginIndex() {
        beginIndex = (pageNo - 1) * pageSize;
        return beginIndex;
    }
}
