package com.fuxiang.web.common;

import lombok.Data;

@Data
public class ErrorInfo {
    //错误编码
    private Integer code;

    //错误消息
    private String message;

    public ErrorInfo(Integer code, String message) {
        this.code = code;
        this.message = message;
    }
}
