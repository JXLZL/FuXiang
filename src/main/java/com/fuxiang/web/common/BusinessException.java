package com.fuxiang.web.common;

import java.util.Arrays;
import java.util.List;

public class BusinessException extends RuntimeException {

    private List<ErrorInfo> errors;

    public BusinessException(List<ErrorInfo> errors) {
        this.errors = errors;
    }

    public BusinessException(List<ErrorInfo> errors,Throwable cause) {
        super(cause);
        this.errors = errors;
    }

    public BusinessException(ErrorInfo ... errors){
        this.errors = Arrays.asList(errors);
    }

    public BusinessException(Integer code, String message){
        this.errors = Arrays.asList(new ErrorInfo(code, message));
    }

    @Override
    public String toString() {
        return "BusinessException{" +
                "errors=" + errors +
                "} " + super.toString();
    }
}
