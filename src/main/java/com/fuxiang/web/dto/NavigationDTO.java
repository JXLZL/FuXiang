package com.fuxiang.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel("导航DTO")
public class NavigationDTO {
    @ApiModelProperty("新闻分类数组")
    private List<NewsCategoryDTO> newsCategoryDTOS;

    @ApiModelProperty("产品分类数组")
    private List<ProductCategoryDTO> productCategoryDTOS;

    @ApiModelProperty("案例分类数组")
    private List<ExampleCategoryDTO> exampleCategoryDTOS;
}
