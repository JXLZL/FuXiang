package com.fuxiang.web.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class BaseDTO {
    @ApiModelProperty("id [32位UUID去横杠]")
    private String id;              //id（32位UUID去横杠）

    @ApiModelProperty("创建时间 [时间戳]")
    private Date createTime;        //创建时间
}
