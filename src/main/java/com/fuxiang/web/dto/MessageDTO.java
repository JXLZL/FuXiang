package com.fuxiang.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@ApiModel(value = "留言DTO")
public class MessageDTO extends BaseDTO {
    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("联系电话")
    private String tel;

    @ApiModelProperty("电子邮箱")
    private String email;

    @ApiModelProperty("联系地址")
    private String address;

    @ApiModelProperty("邮政编码")
    private String zip;

    @ApiModelProperty("留言主题")
    private String subject;

    @ApiModelProperty("留言内容")
    private String content;

    @ApiModelProperty("是否已处理")
    private Boolean dealed;

    @ApiModelProperty("处理时间")
    private Date dealTime;

    @ApiModelProperty("备注")
    private String comment;
}
