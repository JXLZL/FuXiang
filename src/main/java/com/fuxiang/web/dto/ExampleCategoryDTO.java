package com.fuxiang.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("案例分类DTO")
public class ExampleCategoryDTO extends BaseDTO {

    @ApiModelProperty("父节点id")
    private String parentId;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("简介")
    private String intro;

    @ApiModelProperty("图片url")
    private String imageUrl;

    @ApiModelProperty("目录层次")
    private Integer level;

    @ApiModelProperty("排序")
    private Integer sortIndex;

    @ApiModelProperty("是否叶子节点")
    private Boolean leaf;

    @ApiModelProperty("seo标题")
    private String seoTitle;

    @ApiModelProperty("seo关键词")
    private String seoKey;

    @ApiModelProperty("seo描述")
    private String seoDescription;

    @ApiModelProperty("备注")
    private String comment;
}
