package com.fuxiang.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "案例")
public class ExampleDTO extends BaseDTO {

    @ApiModelProperty("类别id")
    private String categoryId;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("简介")
    private String intro;

    @ApiModelProperty("图片url")
    private String imageUrl;

    @ApiModelProperty("大图片url")
    private String bigImageUrl;

    @ApiModelProperty("排序")
    private Integer sortIndex;

    @ApiModelProperty("是否推荐")
    private Boolean recommend;

    @ApiModelProperty("seo标题")
    private String seoTitle;

    @ApiModelProperty("seo关键词")
    private String seoKey;

    @ApiModelProperty("seo描述")
    private String seoDescription;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("备注")
    private String comment;

    @ApiModelProperty("阅读次数")
    private Integer readCount;

    @ApiModelProperty("喜欢次数")
    private Integer likeCount;

}
