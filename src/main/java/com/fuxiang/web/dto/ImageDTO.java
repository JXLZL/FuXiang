package com.fuxiang.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("图片DTO")
public class ImageDTO extends BaseDTO {
    @ApiModelProperty("图片原始名称")
    private String name;

    @ApiModelProperty("图片URL路径")
    private String url;

    @ApiModelProperty("实际存储路径")
    private String realPath;

    @ApiModelProperty("所属模块")
    private String module;

    @ApiModelProperty("引用次数")
    private Integer refCount;

    @ApiModelProperty("是否收藏")
    private Boolean collected;
}
