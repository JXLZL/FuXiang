package com.fuxiang.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("单一页面DTO")
public class SinglePageDTO {
    @ApiModelProperty("名称")
    private String name;                //名称

    @ApiModelProperty("所属导航id")
    private String navId;               //所属导航id

    @ApiModelProperty("排序")
    private Integer sortIndex;          //排序

    @ApiModelProperty("seo标题")
    private String seoTitle;            //seo标题

    @ApiModelProperty("seo关键词")
    private String seoKey;              //seo关键词

    @ApiModelProperty("seo描述")
    private String seoDescription;      //seo描述

    @ApiModelProperty("内容")
    private String content;             //内容

    @ApiModelProperty("备注")
    private String comment;             //备注
}
