package com.fuxiang.web.dto.query;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class BaseQueryDTO {
    @ApiModelProperty("页码 >0")
    private Integer pageNo = 1;

    @ApiModelProperty("页面大小 >0")
    private Integer pageSize = 10;

    @ApiModelProperty("起始下标（不需要填写，后台自动计算）")
    private Integer beginIndex;

    public Integer getBeginIndex() {
        if (pageNo == null || pageSize == null)
            return null;
        beginIndex = (pageNo - 1) * pageSize;
        return beginIndex;
    }
}
