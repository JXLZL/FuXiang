package com.fuxiang.web.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("MessageQueryDTO")
@ApiModel("留言查询DTO")
public class MessageQueryDTO extends BaseQueryDTO {
    @ApiModelProperty("姓名")
    private String name;

    @ApiModelProperty("电话")
    private String tel;

    @ApiModelProperty("邮箱")
    private String email;

    @ApiModelProperty("地址")
    private String address;

    @ApiModelProperty("邮编")
    private String zip;

    @ApiModelProperty("主题")
    private String subject;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("是否已处理")
    private String dealed;

    @ApiModelProperty("备注")
    private String comment;
}
