package com.fuxiang.web.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("NewsQueryDTO")
@ApiModel("新闻查询对象")
public class NewsQueryDTO extends BaseQueryDTO {

    @ApiModelProperty("类别id")
    private String categoryId;

    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("作者")
    private String author;

    @ApiModelProperty("来源")
    private String source;

    @ApiModelProperty("简介")
    private String intro;

    @ApiModelProperty("是否推荐")
    private Boolean recommend;

    @ApiModelProperty("seo标题")
    private String seoTitle;

    @ApiModelProperty("seo关键词")
    private String seoKey;

    @ApiModelProperty("seo描述")
    private String seoDescription;

    @ApiModelProperty("内容")
    private String content;

    @ApiModelProperty("备注")
    private String comment;
}
