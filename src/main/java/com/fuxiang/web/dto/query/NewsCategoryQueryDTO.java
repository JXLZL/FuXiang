package com.fuxiang.web.dto.query;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("NewsCategoryQueryDTO")
@ApiModel("新闻分类查询DTO")
public class NewsCategoryQueryDTO extends BaseQueryDTO {
    @ApiModelProperty("名称")
    private String name;

    @ApiModelProperty("seo标题")
    private String seoTitle;

    @ApiModelProperty("seo关键词")
    private String seoKey;

    @ApiModelProperty("seo描述")
    private String seoDescription;
}
