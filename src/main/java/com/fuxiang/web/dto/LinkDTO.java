package com.fuxiang.web.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel("友情链接DTO")
public class LinkDTO extends BaseDTO {

    @ApiModelProperty("名称 <=300字符")
    private String name;            //名称

    @ApiModelProperty("链接URL <=200字符]")
    private String url;             //链接URL

    @ApiModelProperty("排序")
    private Integer sortIndex;      //排序

    @ApiModelProperty("备注 <=900字符")
    private String comment;         //备注
}
