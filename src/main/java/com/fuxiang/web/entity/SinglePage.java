package com.fuxiang.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("SinglePage")
public class SinglePage extends BaseEntity {
    private String name;                //名称

    private String navId;               //所属导航id

    private Integer sortIndex;          //排序

    private String seoTitle;            //seo标题

    private String seoKey;              //seo关键词

    private String seoDescription;      //seo描述

    private String content;             //内容

    private String comment;             //备注
}
