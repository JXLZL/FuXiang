package com.fuxiang.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("ExampleCategory")
public class ExampleCategory extends BaseEntity {

    private String parentId;            //父节点id

    private String name;                //名称

    private String intro;               //简介

    private String imageUrl;            //图片url

    private Integer level;              //目录层次

    private Integer sortIndex;          //排序

    private Boolean leaf;               //是否叶子节点

    private String seoTitle;            //seo标题

    private String seoKey;              //seo关键词

    private String seoDescription;      //seo描述

    private String comment;             //备注

}
