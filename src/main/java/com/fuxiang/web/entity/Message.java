package com.fuxiang.web.entity;

import lombok.Data;

import java.util.Date;

//留言
@Data
public class Message extends BaseEntity {
    private String name;            //姓名

    private String tel;             //联系电话

    private String email;           //电子邮箱

    private String address;         //联系地址

    private String zip;             //邮政编码

    private String subject;         //留言主题

    private String content;         //留言内容

    private Boolean dealed;         //是否已处理

    private Date dealTime;          //处理时间

    private String comment;         //备注
}
