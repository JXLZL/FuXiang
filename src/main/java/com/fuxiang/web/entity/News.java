package com.fuxiang.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("News")
public class News extends BaseEntity {

    private String categoryId;          //类别id

    private String name;                //名称

    private String author;              //作者

    private String source;              //来源

    private String intro;               //简介

    private String imageUrl;            //图片url

    private String bigImageUrl;         //大图片url

    private Integer sortIndex;          //排序

    private Boolean recommend;          //是否推荐

    private String seoTitle;            //seo标题

    private String seoKey;              //seo关键词

    private String seoDescription;      //seo描述

    private String content;             //内容

    private String comment;             //备注

    private Integer readCount;          //阅读次数

    private Integer likeCount;          //喜欢次数
}
