package com.fuxiang.web.entity;

import lombok.Data;

import java.util.Date;

//系统持久类基类
@Data
public class BaseEntity {
    private String id;              //id（32位UUID去横杠）

    private String createId;        //创建者id

    private Date createTime;        //创建时间

    private String updateId;        //更新者id

    private Date updateTime;        //更新时间

    private Boolean deleted;        //是否删除
}
