package com.fuxiang.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

//友情链接
@Data
@Alias("Link")
public class Link extends BaseEntity {

    private String name;            //名称

    private String url;             //链接URL

    private Integer sortIndex;      //排序

    private String comment;         //备注

}
