package com.fuxiang.web.entity;

import lombok.Data;
import org.apache.ibatis.type.Alias;

@Data
@Alias("Image")
public class Image extends BaseEntity {
    private String name;            //图片原始名称

    private String url;             //图片URL路径

    private String realPath;        //实际存储路径

    private String module;          //所属模块

    private Integer refCount;       //引用次数

    private Boolean collected;      //是否收藏
}
