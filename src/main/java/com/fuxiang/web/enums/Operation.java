package com.fuxiang.web.enums;

public enum Operation {

    FAIL_ADD(1, "新增失败"),
    FAIL_DEL(2, "删除失败"),
    FAIL_UPD(3, "更新失败"),
    FAIL_GET(4, "获取失败");

    private int code;
    private String message;

    Operation(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public int getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}