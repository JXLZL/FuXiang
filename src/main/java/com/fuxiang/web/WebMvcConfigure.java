package com.fuxiang.web;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebMvcConfigure implements WebMvcConfigurer {

    @Value("${upload.image.path}")
    private String imagePath;

    @Value("${upload.image.dir}")
    private String imageDir;

    /**
     * 添加外部静态资源映射
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler(imagePath+"**").addResourceLocations("file:" + imageDir);
        registry.addResourceHandler("/upload/image/**").addResourceLocations("file:D:\\image\\");
    }

    /**
     * 添加跨域请求支持
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowCredentials(true)
                .allowedMethods("GET", "POST", "DELETE", "PUT")
                .maxAge(3600);

    }
}
