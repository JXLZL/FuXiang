package com.fuxiang.web;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.entity.User;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

@Controller
public class UserController {
    @ApiOperation("查询网页")
    @GetMapping("user/{id}.html")
    public ModelAndView getUserHtml(@ApiParam("id") @PathVariable String id) throws IOException {
        System.out.println("当前线程："+Thread.currentThread().getName());
        System.out.println("进入user查询网页...");
        User user = new User();
        user.setId(1);
        user.setName("张珊珊");
        user.setAge(19);
        ModelAndView modelAndView = new ModelAndView("user");
        modelAndView.addObject("user", user);
        modelAndView.addObject("test","just a small test for jsp el");
        return modelAndView;
    }
    @ApiOperation("查询网页")
    @GetMapping("user2/{id}.html")
    public String getUserHtml2(@ApiParam("id") @PathVariable String id, Model model) throws IOException {
        System.out.println("进入user2查询网页...");
        User user = new User();
        user.setId(1);
        user.setName("张珊珊");
        user.setAge(19);
//        ModelAndView modelAndView = new ModelAndView("user");
        model.addAttribute("user", user);
        model.addAttribute("test","just a small test for jsp el");
        return "user2";
    }

    @ApiOperation("测试session")
    @GetMapping("session")
    @ResponseBody
    public ActionResult<String> header(HttpServletRequest request){
        String id = request.getSession().getId();



        return new ActionResult<>(id);
    }

}
