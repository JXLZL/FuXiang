package com.fuxiang.web.util;

import com.fuxiang.web.common.BusinessException;
import com.fuxiang.web.common.SystemContext;
import com.fuxiang.web.entity.BaseEntity;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

public class EntityUtils {

    private static final Logger log = LoggerFactory.getLogger(EntityUtils.class);

    /**
     * 获取UUID（32位无横杠）
     *
     * @return
     */
    public static String uuid() {
        return UUID.randomUUID().toString().replace("-", "");
    }

    public static <T extends BaseEntity> T init(Class<T> tClass) {
        T t = null;
        try {
            t = tClass.newInstance();
        } catch (InstantiationException e) {
            log.error("对象拷贝异常-实例化类" + tClass.getName() + "失败", e);
            e.printStackTrace();
            throw new BusinessException(-1, "对象拷贝异常-实例化类" + tClass.getName() + "失败");
        } catch (IllegalAccessException e) {
            log.error("对象拷贝异常-类" + tClass.getName() + "中有私有的字段", e);
            e.printStackTrace();
            throw new BusinessException(-1, "对象拷贝异常-类" + tClass.getName() + "中有私有的字段");
        }
        return init(t);
    }

    //TODO 有待改进，目前只是测试
    public static <T extends BaseEntity> T init(T t) {
        t.setId(uuid());
        t.setCreateId(SystemContext.getUserId());
        t.setCreateTime(new Date());
        t.setUpdateId(t.getCreateId());
        t.setUpdateTime(t.getCreateTime());
        t.setDeleted(false);
        return t;
    }


    public static <T extends BaseEntity> void update(T t){
        t.setUpdateTime(new Date());
        t.setUpdateId(SystemContext.getUserId());
    }

    /**
     * 拷贝对象a的属性到类B的实例
     *
     * @param a
     * @param bClass
     * @param <A>
     * @param <B>
     * @return
     */
    public static <A, B> B copyObject(A a, Class<B> bClass) {
        if (a == null)
            return null;
        B b = null;
        try {
            b = bClass.newInstance();
        } catch (InstantiationException e) {
            log.error("对象拷贝异常-实例化类" + bClass.getName() + "失败", e);
            e.printStackTrace();
            throw new BusinessException(-1, "对象拷贝异常-实例化类" + bClass.getName() + "失败");
        } catch (IllegalAccessException e) {
            log.error("对象拷贝异常-类" + bClass.getName() + "中有私有的字段", e);
            e.printStackTrace();
            throw new BusinessException(-1, "对象拷贝异常-类" + bClass.getName() + "中有私有的字段");
        }
        BeanUtils.copyProperties(a, b);
        return b;
    }


    public static <A,B> List<B> copyList(List<A> aList, Class<B> bClass){
        if (CollectionUtils.isEmpty(aList))
            return null;
        List<B> bList = new ArrayList<>();
        for (A a : aList){
            try {
                B b = bClass.newInstance();
                BeanUtils.copyProperties(a,b);
                bList.add(b);
            } catch (InstantiationException e) {
                log.error("对象拷贝异常-实例化类" + bClass.getName() + "失败", e);
                e.printStackTrace();
                throw new BusinessException(-1, "对象拷贝异常-实例化类" + bClass.getName() + "失败");
            } catch (IllegalAccessException e) {
                log.error("对象拷贝异常-类" + bClass.getName() + "中有私有的字段", e);
                e.printStackTrace();
                throw new BusinessException(-1, "对象拷贝异常-类" + bClass.getName() + "中有私有的字段");
            }
        }
        return bList;
    }

    /**
     * 拷贝对象a的属性到类B(父类BaseEntity)的实例并进行初始化
     *
     * @param a
     * @param bClass
     * @param <A>
     * @param <B>
     * @return
     * @throws InstantiationException
     * @throws IllegalAccessException
     */
    public static <A, B extends BaseEntity> B copyObjectWithInit(A a, Class<B> bClass) {
        return init(copyObject(a, bClass));
    }


    public static void main(String[] args) {
        System.out.println(uuid());
    }
}
