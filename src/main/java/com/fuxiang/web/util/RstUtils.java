package com.fuxiang.web.util;

import com.fuxiang.web.common.ActionResult;
import com.fuxiang.web.enums.Operation;

//返回工具类
public class RstUtils {
    public static <T> ActionResult<T> ok(T t) {
        return new ActionResult<>(t);
    }

    public static ActionResult noData(String id, Operation operation) {
        return new ActionResult(-1, operation.getMessage() + "，数据不存在，id=" + id);
    }

    public static ActionResult nullError(Object obj, String id, Operation operation) {
        return noData(id, operation);
    }

}

