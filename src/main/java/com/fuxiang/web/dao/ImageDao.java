package com.fuxiang.web.dao;

import com.fuxiang.web.entity.Image;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ImageDao {
    int insert(Image image);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int collect(@Param("id") String id, @Param("updateId") String updateId);

    int ref(@Param("id") String id, @Param("updateId") String updateId);

    Image get(String id);

    int countByModuleOrCollected(@Param("module") String module, @Param("collected") Boolean collected);

    List<Image> listByModuleOrCollected(@Param("module") String module,
                                        @Param("collected") Boolean collected,
                                        @Param("beginIndex") Integer beginIndex,
                                        @Param("pageSize") Integer pageSize);
}
