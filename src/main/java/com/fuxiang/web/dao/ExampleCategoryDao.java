package com.fuxiang.web.dao;


import com.fuxiang.web.entity.ExampleCategory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExampleCategoryDao {
    int insert(ExampleCategory exampleCategory);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(ExampleCategory exampleCategory);

    ExampleCategory get(String id);

    Integer getMaxSortIndex(@Param("parentId") String parentId);

    int countByParentId(@Param("parentId") String parentId);

    //beginIndex和pageSize设为null查询全部
    List<ExampleCategory> listByParentId(@Param("parentId") String parentId,
                                         @Param("beginIndex") Integer beginIndex,
                                         @Param("pageSize") Integer pageSize);

    int batchUpdateSortIndex(@Param("list") List<ExampleCategory> list,
                             @Param("updateId") String updateId);

    List<ExampleCategory> listAll();
}
