package com.fuxiang.web.dao;


import com.fuxiang.web.entity.ProductCategory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductCategoryDao {
    int insert(ProductCategory productCategory);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(ProductCategory productCategory);

    ProductCategory get(String id);

    Integer getMaxSortIndex(@Param("parentId") String parentId);

    int countByParentId(@Param("parentId") String parentId);

    //beginIndex和pageSize设为null查询全部
    List<ProductCategory> listByParentId(@Param("parentId") String parentId,
                                         @Param("beginIndex") Integer beginIndex,
                                         @Param("pageSize") Integer pageSize);

    int batchUpdateSortIndex(@Param("list") List<ProductCategory> list,
                             @Param("updateId") String updateId);

    List<ProductCategory> listAll();
}
