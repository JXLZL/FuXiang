package com.fuxiang.web.dao;


import com.fuxiang.web.entity.NewsCategory;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsCategoryDao {
    int insert(NewsCategory newsCategory);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(NewsCategory newsCategory);

    NewsCategory get(String id);

    Integer getMaxSortIndex(@Param("parentId") String parentId);

    int countByParentId(@Param("parentId") String parentId);

    //beginIndex和pageSize设为null查询全部
    List<NewsCategory> listByParentId(@Param("parentId") String parentId,
                                         @Param("beginIndex") Integer beginIndex,
                                         @Param("pageSize") Integer pageSize);

    int batchUpdateSortIndex(@Param("list") List<NewsCategory> list,
                             @Param("updateId") String updateId);


    List<NewsCategory> listAll();
}
