package com.fuxiang.web.dao;


import com.fuxiang.web.entity.News;
import com.fuxiang.web.dto.query.NewsQueryDTO;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface NewsDao {
    int insert(News news);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(News news);

    News get(String id);

    Integer getMaxSortIndex(String categoryId);

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int batchUpdateSortIndex(@Param("pList") List<News> pList, @Param("updateId") String updateId);

    List<News> listAllByCategoryId(String categoryId);

    int countByQueryDTO(NewsQueryDTO queryDTO);

    List<News> listByQueryDTO(NewsQueryDTO queryDTO);

}
