package com.fuxiang.web.dao;


import com.fuxiang.web.dto.query.ExampleQueryDTO;
import com.fuxiang.web.entity.Example;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ExampleDao {
    int insert(Example example);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(Example example);

    Example get(String id);

    Integer getMaxSortIndex(String categoryId);

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int batchUpdateSortIndex(@Param("pList") List<Example> pList, @Param("updateId") String updateId);

    List<Example> listAllByCategoryId(String categoryId);

    int countByQueryDTO(ExampleQueryDTO queryDTO);

    List<Example> listByQueryDTO(ExampleQueryDTO queryDTO);

}
