package com.fuxiang.web.dao;

import com.fuxiang.web.dto.query.MessageQueryDTO;
import com.fuxiang.web.entity.Message;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface MessageDao {
    int insert(Message message);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(Message message);

    Message get(String id);

    List<Message> listAll();

    int batchDelete(@Param("idList") List<String> ids, @Param("updateId") String updateId);

    int deal(@Param("id") String id, @Param("updateId") String updateId);

    int batchDeal(@Param("idList") List<String> ids, @Param("updateId") String updateId);

    List<Message> listByQueryDTO(MessageQueryDTO queryDTO);

    int countByQueryDTO(MessageQueryDTO queryDTO);
}
