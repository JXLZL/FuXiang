package com.fuxiang.web.dao;

import com.fuxiang.web.entity.Link;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface LinkDao {
    int insert(Link link);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(Link link);

    Link get(String id);

    List<Link> list();

    Integer getMaxSortIndex();

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int batchUpdateSortIndex(@Param("list") List<Link> list, @Param("updateId") String updateId);

}
