package com.fuxiang.web.dao;


import com.fuxiang.web.dto.query.ProductQueryDTO;
import com.fuxiang.web.entity.Product;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ProductDao {
    int insert(Product product);

    int delete(@Param("id") String id, @Param("updateId") String updateId);

    int update(Product product);

    Product get(String id);

    Integer getMaxSortIndex(String categoryId);

    int batchDelete(@Param("idList") List<String> idList, @Param("updateId") String updateId);

    int batchUpdateSortIndex(@Param("productList") List<Product> productList, @Param("updateId") String updateId);

    int countByCategoryId(String categoryId);

    List<Product> listAllByCategoryId(String categoryId);

    int countByQueryDTO(ProductQueryDTO queryDTO);

    List<Product> listByQueryDTO(ProductQueryDTO queryDTO);

}
