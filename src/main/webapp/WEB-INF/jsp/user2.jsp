<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%--开启el表达式，也可以修改web.xml文件头改为2.5或以上版本会自动开启el表达式--%>
<%--<%@ page isELIgnored="false" %>--%>
<html>
<head>
    <title>第二张测试用户页面jsp</title>
</head>
<body>
<table align="center" border="1" cellspacing="0" cellpadding="15" style="border-radius:5px" width="50%">
    <caption style="background-color: cadetblue;font-family: 'Microsoft JhengHei';font-size: larger">已添加用户的属性</caption>
    <tbody>
    <tr>
        <td style="text-align: center; background-color: azure">id</td>
        <td>${user.id}</td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: azure">姓名</td>
        <td>${user.name}</td>
    </tr>
    <tr>
        <td style="text-align: center; background-color: azure">年龄</td>
        <td>${user.age}</td>
    </tr>
    </tbody>

</table>
<h1 align="center">测试${test}</h1>

</body>
</html>