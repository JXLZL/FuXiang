package com.fuxiang.web.dao;

import com.fuxiang.web.entity.NewsCategory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

@SpringBootTest
@RunWith(SpringRunner.class)
@MapperScan("com.fuxiang.yw.web.dao")
public class NewsCategoryDaoTest {

    @Autowired
    private NewsCategoryDao newsCategoryDao;

    @Test
    public void listAll() {
        List<NewsCategory> newsCategories = newsCategoryDao.listAll();
        for (NewsCategory newsCategory : newsCategories)
            System.out.println(newsCategory);
    }
}