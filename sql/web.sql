CREATE DATABASE yw_fuxiang_web_develop DEFAULT CHARACTER
SET utf8 COLLATE utf8_unicode_ci;
USE yw_fuxiang_web_develop;

CREATE TABLE t_link (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '名称',
`url` VARCHAR ( 200 ) NOT NULL COMMENT '链接URL',
`sort_index` TINYINT ( 4 ) NOT NULL COMMENT '排序',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '友情链接表';

CREATE TABLE t_image (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '图片名称',
`url` VARCHAR ( 200 ) NOT NULL COMMENT '图片URL',
`real_path` VARCHAR ( 300 ) NOT NULL COMMENT '物理存储路径',
`module` VARCHAR ( 30 ) DEFAULT NULL COMMENT '所属模块',
`ref_count` int NOT NULL DEFAULT 1 COMMENT '引用次数',
`collected` bit ( 1 ) NOT NULL DEFAULT 0 COMMENT '是否收藏：0否 1是',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '图片表';

CREATE TABLE t_message (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`name` VARCHAR ( 30 ) NOT NULL COMMENT '姓名',
`tel` VARCHAR ( 30 ) NOT NULL COMMENT '电话',
`email` VARCHAR ( 30 ) NOT NULL COMMENT '邮箱',
`address` VARCHAR (100) DEFAULT NULL COMMENT '地址',
`zip` VARCHAR (10) DEFAULT NULL COMMENT '邮编',
`subject` VARCHAR ( 300 ) DEFAULT NULL COMMENT '主题',
`content` TEXT NOT NULL COMMENT '内容',
`dealed` bit ( 1 ) NOT NULL DEFAULT 0 COMMENT '是否已处理：0否 1是',
`deal_time` datetime DEFAULT NULL COMMENT '处理时间',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '留言表';


CREATE TABLE t_product_category (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`parent_id` VARCHAR ( 32 ) NOT NULL COMMENT '父节点id',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '名称',
`intro` VARCHAR ( 900 ) DEFAULT NULL COMMENT '简介',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`level` TINYINT NOT NULL COMMENT '目录层级',
`sort_index` TINYINT NOT NULL COMMENT '排序',
`leaf` bit NOT NULL COMMENT '是否叶子节点：0否 1是',
`seo_title` VARCHAR ( 300 ) DEFAULT NULL COMMENT 'seo标题',
`seo_key` VARCHAR ( 600 ) DEFAULT NULL COMMENT 'seo关键词',
`seo_description` VARCHAR ( 900 ) DEFAULT NULL COMMENT 'seo描述',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '产品分类表';

CREATE TABLE t_product (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`category_id` VARCHAR ( 32 ) DEFAULT NULL COMMENT '分类id',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '名称',
`intro` VARCHAR ( 900 ) DEFAULT NULL COMMENT '简介',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`big_image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '大图片url',
`sort_index` TINYINT NOT NULL COMMENT '排序',
`recommend` bit NOT NULL COMMENT '是否推荐：0否 1是',
`seo_title` VARCHAR ( 300 ) DEFAULT NULL COMMENT 'seo标题',
`seo_key` VARCHAR ( 600 ) DEFAULT NULL COMMENT 'seo关键词',
`seo_description` VARCHAR ( 900 ) DEFAULT NULL COMMENT 'seo描述',
`content` TEXT DEFAULT NULL COMMENT '内容',
`content2` TEXT DEFAULT NULL COMMENT '副内容',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
`read_count` int DEFAULT 0 COMMENT '阅读次数',
`like_count` int DEFAULT 0 COMMENT '喜欢次数',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '产品表';

CREATE TABLE t_news_category (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`parent_id` VARCHAR ( 32 ) NOT NULL COMMENT '父节点id',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '名称',
`intro` VARCHAR ( 900 ) DEFAULT NULL COMMENT '简介',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`level` TINYINT NOT NULL COMMENT '目录层级',
`sort_index` TINYINT NOT NULL COMMENT '排序',
`leaf` bit NOT NULL COMMENT '是否叶子节点：0否 1是',
`seo_title` VARCHAR ( 300 ) DEFAULT NULL COMMENT 'seo标题',
`seo_key` VARCHAR ( 600 ) DEFAULT NULL COMMENT 'seo关键词',
`seo_description` VARCHAR ( 900 ) DEFAULT NULL COMMENT 'seo描述',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '新闻分类表';

CREATE TABLE t_news (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`category_id` VARCHAR ( 32 ) DEFAULT NULL COMMENT '分类id',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '名称',
`author` VARCHAR ( 300 ) DEFAULT NULL COMMENT '作者',
`source` VARCHAR ( 300 ) DEFAULT NULL COMMENT '来源',
`intro` VARCHAR ( 900 ) DEFAULT NULL COMMENT '简介',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`big_image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '大图片url',
`sort_index` TINYINT NOT NULL COMMENT '排序',
`recommend` bit NOT NULL COMMENT '是否推荐：0否 1是',
`seo_title` VARCHAR ( 300 ) DEFAULT NULL COMMENT 'seo标题',
`seo_key` VARCHAR ( 600 ) DEFAULT NULL COMMENT 'seo关键词',
`seo_description` VARCHAR ( 900 ) DEFAULT NULL COMMENT 'seo描述',
`content` TEXT DEFAULT NULL COMMENT '内容',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
`read_count` int DEFAULT 0 COMMENT '阅读次数',
`like_count` int DEFAULT 0 COMMENT '喜欢次数',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '新闻表';

CREATE TABLE t_example_category (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`parent_id` VARCHAR ( 32 ) NOT NULL COMMENT '父节点id',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '名称',
`intro` VARCHAR ( 900 ) DEFAULT NULL COMMENT '简介',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`level` TINYINT NOT NULL COMMENT '目录层级',
`sort_index` TINYINT NOT NULL COMMENT '排序',
`leaf` bit NOT NULL COMMENT '是否叶子节点：0否 1是',
`seo_title` VARCHAR ( 300 ) DEFAULT NULL COMMENT 'seo标题',
`seo_key` VARCHAR ( 600 ) DEFAULT NULL COMMENT 'seo关键词',
`seo_description` VARCHAR ( 900 ) DEFAULT NULL COMMENT 'seo描述',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '案例分类表';

CREATE TABLE t_example (
`id` VARCHAR ( 32 ) NOT NULL COMMENT '主键',
`create_id` VARCHAR ( 32 ) NOT NULL COMMENT '创建者id',
`create_time` datetime NOT NULL COMMENT '创建时间',
`update_id` VARCHAR ( 32 ) NOT NULL COMMENT '更新者id',
`update_time` datetime NOT NULL COMMENT '更新时间',
`deleted` bit NOT NULL DEFAULT 0 COMMENT '是否删除：0否 1是',
`category_id` VARCHAR ( 32 ) DEFAULT NULL COMMENT '分类id',
`name` VARCHAR ( 300 ) NOT NULL COMMENT '名称',
`intro` VARCHAR ( 900 ) DEFAULT NULL COMMENT '简介',
`image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '图片url',
`big_image_url` VARCHAR ( 200 ) DEFAULT NULL COMMENT '大图片url',
`sort_index` TINYINT NOT NULL COMMENT '排序',
`recommend` bit NOT NULL COMMENT '是否推荐：0否 1是',
`seo_title` VARCHAR ( 300 ) DEFAULT NULL COMMENT 'seo标题',
`seo_key` VARCHAR ( 600 ) DEFAULT NULL COMMENT 'seo关键词',
`seo_description` VARCHAR ( 900 ) DEFAULT NULL COMMENT 'seo描述',
`content` TEXT DEFAULT NULL COMMENT '内容',
`comment` VARCHAR ( 900 ) DEFAULT NULL COMMENT '备注',
`read_count` int DEFAULT 0 COMMENT '阅读次数',
`like_count` int DEFAULT 0 COMMENT '喜欢次数',
PRIMARY KEY ( id )
) ENGINE = INNODB DEFAULT charset utf8 COMMENT '案例表';

